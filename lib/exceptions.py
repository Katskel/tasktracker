"""
    Provides all library custom exceptions

"""


class TaskNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "Task " + str(self.value) + " not found"


class PermissionDenied(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return "Permission denied : " + self.message


class UserNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "User " + str(self.value) + " not found"


class GroupNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "Group " + str(self.value) + " not found"


class PlanNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "Plan " + str(self.value) + " not found"


class TaskGroupNotFound(Exception):
    def __init__(self, task_id, group_id):
        self.task_id = task_id
        self.group_id = group_id

    def __str__(self):
        return "Group" + str(self.group_id) + "doesn't have task" + str(self.task_id)


class TaskActionNotFound(Exception):
    def __init__(self, task_id, user_login):
        self.task_id = task_id
        self.user_login = user_login

    def __str__(self):
        return "User " + str(self.user_login) + " has no actions with task " + str(self.task_id)


class CommandNotFound(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return "Command not found: " + self.message


class InvalidConfig(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return 'Invalid config: ' + self.message


class ConfigNotFound(Exception):
    def __str__(self):
        return 'Config not found'

