"""
    Application models

    Modules:
        task - task entity
        access - class that binds users with their tasks
        group - groups entity
        task_planner - entity that creates tasks by some plan

"""