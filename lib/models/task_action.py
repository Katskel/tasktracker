"""
    Provides class that shows users' permissions to tasks

    Classes:
        TaskAction - link between task and user (action that can user do with task)
        ActionType - values for actions with tasks

"""

from enum import Enum


class TaskAction:
    def __init__(self, task_id, user_login, action_type=0):
        self.task_id = task_id
        self.user_login = user_login
        self.action_type = ActionType(action_type)


class ActionType(Enum):
    READ = 0
    WRITE = 1

    def __str__(self):
        return str(self.name).lower().replace("_", " ").capitalize()
