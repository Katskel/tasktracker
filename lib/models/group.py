"""
    Provides classes to work with groups

    Classes:
        Group - group description task
        TaskGroup - class contain link between tasks and their groups

"""


class Group:
    """
        User group description

        Values:
            id - group id in system
            name - group name
            user_login - group's owner login

    """

    def __init__(self, group_id, name, user_login):
        self.id = group_id
        self.name = name
        self.user_login = user_login


class TaskGroup:
    """
        Link that shows that group contains some task

        Values:
            group_id - id of group that contains task
            task_id - id of contained in group task

    """

    def __init__(self, task_id, group_id):
        self.task_id = task_id
        self.group_id = group_id
