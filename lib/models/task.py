"""
    Provides classes with task description

    Classes:
        Task - main task class
        TaskPriority - class with values of task priority
        TaskStatus - class with values of task status

"""

from enum import Enum


class Task:
    """
        Class with information about task

        Values:
            id - task id in system
            name - name of task
            description - description of task
            reminder_time - time in which the task will be shown to user as active
            end_date - last date to complete task, after task will be failed
            status - current status of task
            priority - current status of task
            head_task_id - id of parent task (parent task has this task as subtask)
            is_archive - shows is it task in the archive (true/false)

    """

    def __init__(self, task_id, name, description='', reminder_time=None, end_date=None, priority=0, head_task_id=None, is_archive=False, status=1):
        self.id = task_id
        self.name = name
        self.description = description
        self.reminder_time = reminder_time
        self.end_date = end_date
        self.status = TaskStatus(status)
        self.priority = TaskPriority(priority)
        self.head_task_id = head_task_id
        self.is_archive = is_archive


class TaskPriority(Enum):
    """Task priority values (value of Task.priority)"""

    NOT = 0
    LOW = 1
    MEDIUM = 2
    HIGH = 3

    def __str__(self):
        return str(self.name).lower().replace("_", " ").capitalize()


class TaskStatus(Enum):
    """Task status values (value of Task.status)"""

    NOT_STARTED = 0
    IN_PROCESS = 1
    DONE = 2
    FAILED = 3

    def __str__(self):
        return str(self.name).lower().replace("_", " ").capitalize()