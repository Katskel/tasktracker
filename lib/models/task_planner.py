"""
    Provides class that creates tasks by some plan

    Classes:
        TaskPlanner - user plan for creating task

"""


class TaskPlanner:
    """
        User plan for creating task

        Values:
            id - plan id in system
            task_name - name for task created with plan
            end_date - last date to complete task created with plan, after task will be failed
            reminder_time - time in which the created task will be shown to user as active
            task_description - description of task created with plan
            task_priority - priority of task created with plan
            last_added - last date of the last task created with plan
            repeat_days - the days in which plan will create tasks
            repeat_count - number of times that plan will create task
            repeat_time_delta - plan will create task every time interval (higher priority that repeat_days)
            user_login - user, owner of plan

    """

    def __init__(self, plan_id, task_name, end_date, user_login, repeat_days=None, repeat_count=None,
                 repeat_time_delta=None, reminder_time=None, task_description='', task_priority=0, last_added=None):
        self.id = plan_id
        self.task_name = task_name
        self.end_date = end_date
        self.reminder_time = reminder_time
        self.task_description = task_description
        self.task_priority = task_priority
        self.last_added = last_added
        self.repeat_days = repeat_days
        self.repeat_count = repeat_count
        self.repeat_time_delta = repeat_time_delta
        self.user_login = user_login
