"""
    Interface for working with storage

    Classes:
        Storage - class with storage interface functions

"""

from lib.storage.json_storage import JsonStorage


class Storage:
    """
        Class with storage interface functions

        Values:
            storage - pointer to concrete storage worker

    """

    def __init__(self, path):
        """
            Init concrete storage worker

            Args:
                path - path of data directory

        """
        self.storage = JsonStorage(path)

    def clear_storage(self):
        """
            Clear library storage
        """

        self.storage.clear_storage()

    def get_task_by_id(self, task_id):
        """
            Returns task with given id from storage

            Args:
                task_id - required task id

            Returns:
                lib.models.task.Task object of task

        """

        return self.storage.get_task_by_id(task_id)

    def check_task_action(self, task_id, user_login):
        """
            Return user's type action with task

            Args:
                task_id - required task id
                user_login - user that require task

            Returns:
                lib.models.task_action.TaskAction object of action
        """

        return self.storage.check_task_action(task_id, user_login)

    def update_task(self, task_id, task):
        """
            Update task information in storage

            Args:
                task_id - updated task id
                task - new lib.models.task.Task task object
        """

        self.storage.update_task(task_id, task)

    def delete_task(self, task_id):
        """
            Delete task from storage

            Args:
                task_id - deleted task id

        """

        self.storage.delete_task(task_id)

    def get_user_tasks(self, user_login, name=False, description=False, end_date=False, status=False, priority=False, is_archive=None):
        """
            Returns all users tasks that satisfies some filter

            Args:
                user_login - user login that require tasks
                filter - lambda function, filter task with some properties

            Returns:
                 list of lib.models.task.Task objects - user tasks

        """
        return self.storage.get_user_tasks(user_login=user_login,
                                           name=name,
                                           description=description,
                                           end_date=end_date,
                                           status=status,
                                           priority=priority,
                                           is_archive=is_archive)

    def add_task(self, task):
        """
            Add task to storage

            Args:
                task - lib.models.task.Task object of added task

        """

        self.storage.add_task(task)

    def add_task_action(self, task_action):
        """
            Add task action to storage

            Args:
                task_action - lib.models.task_action.TaskAction object of added action

        """

        self.storage.add_task_action(task_action)

    def delete_task_action(self, task_id, user_login):
        """
            Delete task action from storage

            Args:
                task_id - id of required task
                user_login - user login that lost action

        """

        self.storage.delete_task_action(task_id, user_login)

    def update_task_action(self, task_id, user_login, task_action):
        """
            Update task action information in storage

            Args:
                task_id - task id
                user_login - user that will have new action to task
                task_action - new lib.models.task_action.TaskAction object

        """

        self.storage.update_task_action(task_id, user_login, task_action)

    def get_task_subtasks(self, task_id):
        """
            Returns all subtasks of required task

            Args:
                task_id - required task id

            Returns:
                list of lib.models.task.Task objects - task subtasks

        """

        return self.storage.get_task_subtasks(task_id)

    def get_max_task_id(self):
        """
            Returns max id of created tasks

            Returns:
                 max id (int type)

        """

        return self.storage.get_max_task_id()

    def get_max_group_id(self):
        """
            Returns max id of created groups

            Returns:
                 max id (int type)

        """

        return self.storage.get_max_group_id()

    def add_group(self, group):
        """
            Add group to storage

            Args:
                group - lib.models.group.Group object

        """

        self.storage.add_group(group)

    def update_group(self, group_id, group):
        """
            Update information about group

            Args:
                group_id - id of updated group
                group - new lib.models.group.Group object

        """

        self.storage.update_group(group_id, group)

    def delete_group(self, group_name, user_login):
        """
            Delete group from storage

            Args:
                group_name - name of user group
                user_login - user, owner of group

        """

        self.storage.delete_group(group_name, user_login)

    def get_user_groups(self, user_login):
        """
            Returns all user groups

            Args:
                user_login - required user login

            Returns:
                list of lib.models.group.Group objects - user groups

        """

        return self.storage.get_user_groups(user_login)

    def get_group_by_name(self, group_name, user_login):
        """
            Return one user group by name

            Args:
                group_name - name of required group
                user_login - user that requires group

            Returns:
                lib.models.group.Group object - user group

        """

        return self.storage.get_group_by_name(group_name, user_login)

    def get_group_tasks(self, user_login, group_name):
        """
            Returns all group tasks

            Args:
                user_login - user, owner of group
                group_name - name of required group

            Returns:
                list of lib.models.task.Task objects - group tasks

        """

        return self.storage.get_group_tasks(user_login, group_name)

    def add_task_group(self, task_group):
        """
            Add link between task and group to storage

            Args:
                task_group - lib.models.group.TaskGroup object

        """

        self.storage.add_task_group(task_group)

    def delete_task_group(self, task_id, group_id):
        """
            Delete link between task and group from storage

            Args:
                task_id - linked task id
                group_id - linked group id

        """

        self.storage.delete_task_group(task_id, group_id)

    def get_user_plans(self, user_login):
        """
            Returns all user created plans

            Args:
                user_login - user that requires plans

            Returns:
                list of lib.models.task_planner.TaskPlanner objects - user plans

        """

        return self.storage.get_user_plans(user_login)

    def add_plan(self, plan):
        """
            Add plan to storage

            Args:
                plan - lib.models.task_planner.TaskPlanner object

        """

        self.storage.add_plan(plan)

    def update_plan(self, plan_id, plan):
        """
            Update plan information

            Args:
                plan_id - required plan id
                plan - lib.models.task_planner.TaskPlanner object - new plan

        """

        self.storage.update_plan(plan_id, plan)

    def delete_plan(self, plan_id):
        """
            Delete plan with required id

            Args:
                plan_id - required plan id

        """

        self.storage.delete_plan(plan_id)

    def get_plan_by_id(self, plan_id):
        """
            Return plan with required id

            Args:
                plan_id - required plan id

            Returns:
                lib.models.task_planner.TaskPlanner object - required plan

        """

        return self.storage.get_plan_by_id(plan_id)

    def get_max_plan_id(self):
        """
            Returns max plan created id

            Returns:
                max id (int type)

        """

        return self.storage.get_max_plan_id()
