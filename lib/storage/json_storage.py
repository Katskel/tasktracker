"""
    Module with JSON storage work

    Classes:
        JSONPath - const names for json storage files
        JsonStorage - class that works with json

    Functions:
        load_data(path)
        save_data(path, data)
        create_json_empty_file(path)

"""


import os
import shutil
from json_tricks import dump, load
from lib.exceptions import (
    TaskNotFound,
    TaskActionNotFound,
    GroupNotFound,
    TaskGroupNotFound,
    PlanNotFound,
)


class JSONPath:
    """
        Const name for json storage files
    """

    TASKS = 'tasks.json'
    GROUPS = 'groups.json'
    TASK_ACTIONS = 'task_actions.json'
    TASK_GROUPS = 'tasks_groups.json'
    MAX_ID = 'max_id.json'
    PLANS = 'plans.json'


class JsonStorage:
    """
        Class that works with json storage
    """

    def __init__(self, path):
        """
            Init class object

            Args:
                path - path to storage directory

        """

        self.dir_path = path
        self.prepare_data_directory()
        self.max_id = dict()
        self.load_max_id()

    def prepare_data_directory(self):
        """
            Create directory (if not exists) and storage files(if they not exist)

        """

        if not os.path.exists(self.dir_path):
            os.mkdir(self.dir_path)

        if not os.path.exists(os.path.join(self.dir_path, JSONPath.TASKS)):
            create_json_empty_file(os.path.join(self.dir_path, JSONPath.TASKS))
        if not os.path.exists(os.path.join(self.dir_path, JSONPath.GROUPS)):
            create_json_empty_file(os.path.join(self.dir_path, JSONPath.GROUPS))
        if not os.path.exists(os.path.join(self.dir_path, JSONPath.TASK_GROUPS)):
            create_json_empty_file(os.path.join(self.dir_path, JSONPath.TASK_GROUPS))
        if not os.path.exists(os.path.join(self.dir_path, JSONPath.TASK_ACTIONS)):
            create_json_empty_file(os.path.join(self.dir_path, JSONPath.TASK_ACTIONS))
        if not os.path.exists(os.path.join(self.dir_path, JSONPath.PLANS)):
            create_json_empty_file(os.path.join(self.dir_path, JSONPath.PLANS))
        if not os.path.exists(os.path.join(self.dir_path, JSONPath.MAX_ID)):
            self.max_id = dict(task=0, group=0, plan=0)
            self.save_max_id()

    def clear_storage(self):
        """
            Clear library storage
        """

        shutil.rmtree(self.dir_path)

    def get_task_by_id(self, task_id):
        """
            Returns task with given id from storage

            Args:
                task_id - required task id

            Returns:
                lib.models.task.Task object of task

            Raises:
                TaskNotFound exception, is task not in storage
        """

        tasks = self.load_tasks()
        for task in tasks:
            if task.id == task_id:
                return task
        raise TaskNotFound(task_id)

    def check_task_action(self, task_id, user_login):
        """
            Return user's type of action to task

            Args:
                task_id - required task id
                user_login - user that require task

            Returns:
                lib.models.task_action.TaskAction object of action, None if action not in storage

        """

        task_actions = self.load_task_actions()
        for task_action in task_actions:
            if task_action.task_id == task_id and task_action.user_login == user_login:
                return task_action.action_type
        return None

    def update_task(self, task_id, task):
        """
            Update task information in storage

            Args:
                task_id - updated task id
                task - new lib.models.task.Task task object

            Raises:
                TaskNotFound exception if task not in storage
        """

        tasks = self.load_tasks()
        for i in range(len(tasks)):
            if tasks[i].id == task_id:
                tasks[i] = task
                self.save_tasks(tasks)
                return
        raise TaskNotFound(task_id)

    def get_user_tasks(self, user_login, name=False, description=False, end_date=False, status=False, priority=False, is_archive=None):
        """
            Returns all users tasks that satisfies some filter

            Args:
                user_login - user login that require tasks
                filter - lambda function, filter task with some properties

            Returns:
                 list of lib.models.task.Task objects - user tasks

        """

        task_actions = self.load_task_actions()
        user_tasks = []
        for task_action in task_actions:
            if task_action.user_login == user_login:
                task = self.get_task_by_id(task_action.task_id)
                if name is not False:
                    if name.upper() not in task.name.upper():
                        continue
                if description is not False and task.description != description:
                    continue
                if end_date is not False and task.end_date != end_date:
                    continue
                if status is not False and task.status != status:
                    continue
                if priority is not False and task.priority != priority:
                    continue
                if is_archive is not None and is_archive != task.is_archive:
                    continue
                user_tasks.append(task)
        return user_tasks

    def add_task(self, task):
        """
            Add task to storage

            Args:
                task - lib.models.task.Task object of added task

        """

        tasks = self.load_tasks()
        tasks.append(task)
        self.max_id['task'] += 1
        self.save_max_id()
        self.save_tasks(tasks)

    def add_task_action(self, task_action):
        """
            Add access to storage

            Args:
                task_action - lib.models.task_action.TaskAction object of added action

        """

        task_actions = self.load_task_actions()
        task_actions.append(task_action)
        self.save_task_actions(task_actions)

    def delete_task_action(self, task_id, user_login):
        """
            Delete task action from storage

            Args:
                task_id - id of required task
                user_login - user login that lost task_action

            Raises:
                TaskActionNotFound exception if task action not in storage

        """

        task_actions = self.load_task_actions()
        for task_action in task_actions:
            if task_action.task_id == task_id and task_action.user_login == user_login:
                task_actions.remove(task_action)
                self.save_task_actions(task_actions)
                return
        raise TaskActionNotFound(task_id, user_login)

    def update_task_action(self, task_id, user_login, task_action):
        """
            Update task action information in storage

            Args:
                task_id - task id
                user_login - user that will have new access to task
                task_action - new lib.models.task_action.TaskAction object

            Raises:
                TaskActionNotFound exception if action not in storage

        """

        task_actions = self.load_task_actions()
        for i in range(len(task_actions)):
            if task_actions[i].task_id == task_id and task_actions[i].user_login == user_login:
                task_actions[i].task_access = task_action
                self.save_task_actions(task_actions)
                return
        raise TaskActionNotFound(task_id, user_login)

    def get_task_subtasks(self, task_id):
        """
            Returns all subtasks of required task

            Args:
                task_id - required task id

            Returns:
                list of lib.models.task.Task objects - task subtasks

        """

        tasks = self.load_tasks()
        task_subtasks = []
        for task in tasks:
            if task.head_task_id == task_id:
                task_subtasks.append(task)
        return task_subtasks

    def add_group(self, group):
        """
            Add group to storage

            Args:
                group - lib.models.group.Group object

        """

        groups = self.load_groups()
        groups.append(group)
        self.max_id['group'] += 1
        self.save_max_id()
        self.save_groups(groups)

    def update_group(self, group_id, group):
        """
            Update information about group

            Args:
                group_id - id of updated group
                group - new lib.models.group.Group object

            Raises:
                GroupNotFound exception if group not in storage
        """

        groups = self.load_groups()
        for i in range(len(groups)):
            if groups[i].id == group_id:
                groups[i] = group
                self.save_groups(groups)
                return
        raise GroupNotFound(group_id)

    def delete_group(self, group_name, user_login):
        """
            Delete group from storage

            Args:
                group_name - name of user group
                user_login - user, owner of group

            Raises:
                GroupNotFound exception if group not in storage

        """

        groups = self.load_groups()
        for i in range(len(groups)):
            if groups[i].name == group_name and groups[i].user_login == user_login:
                groups.remove(groups[i])
                self.save_groups(groups)
                return
        raise GroupNotFound(group_name)

    def get_user_groups(self, user_login):
        """
            Returns all user groups

            Args:
                user_login - required user login

            Returns:
                list of lib.models.group.Group objects - user groups

        """

        groups = self.load_groups()
        user_groups = []
        for group in groups:
            if group.user_login == user_login:
                user_groups.append(group)
        return user_groups

    def get_group_by_name(self, group_name, user_login):
        """
            Return one user group by name

            Args:
                group_name - name of required group
                user_login - user that requires group

            Returns:
                lib.models.group.Group object - user group

            Raises:
                GroupNotFound exception if group not in storage

        """

        groups = self.load_groups()
        for group in groups:
            if group.name == group_name and group.user_login == user_login:
                return group
        raise GroupNotFound(group_name)

    def get_group_tasks(self, user_login, group_name):
        """
            Returns all group tasks

            Args:
                user_login - user, owner of group
                group_name - name of required group

            Returns:
                list of lib.models.task.Task objects - group tasks

        """

        group = self.get_group_by_name(group_name, user_login)
        group_id = group.id
        group_tasks = []
        task_groups = self.load_task_groups()
        for task_group in task_groups:
            if task_group.group_id == group_id:
                task = self.get_task_by_id(task_group.task_id)
                group_tasks.append(task)
        return group_tasks

    def add_task_group(self, task_group):
        """
            Add link between task and group to storage

            Args:
                task_group - lib.models.group.TaskGroup object

        """

        task_groups = self.load_task_groups()
        task_groups.append(task_group)
        self.save_task_groups(task_groups)

    def delete_task_group(self, task_id, group_id):
        """
            Delete link between task and group from storage

            Args:
                task_id - linked task id
                group_id - linked group id

            Raises:
                TaskGroupNotFound exception if task_group not in storage

        """

        task_groups = self.load_task_groups()
        for i in range(len(task_groups)):
            if task_groups[i].task_id == task_id and task_groups[i].group_id == group_id:
                task_groups.remove(task_groups[i])
                self.save_task_groups(task_groups)
                return
        raise TaskGroupNotFound(task_id, group_id)

    def delete_task(self, task_id):
        """
            Delete task from storage

            Args:
                task_id - deleted task id

            Raises:
                TaskNotFound exception if task not in storage

        """

        tasks = self.load_tasks()
        task_groups = self.load_task_groups()
        for i in reversed(range(len(task_groups))):
            if task_groups[i].task_id == task_id:
                task_groups.remove(task_groups[i])
        self.save_task_groups(task_groups)
        accesses = self.load_task_actions()
        for i in reversed(range(len(accesses))):
            if accesses[i].task_id == task_id:
                accesses.remove(accesses[i])
        self.save_task_actions(accesses)
        for i in range(len(tasks)):
            if tasks[i].id == task_id:
                tasks.remove(tasks[i])
                self.save_tasks(tasks)
                return
        raise TaskNotFound(task_id)

    def load_tasks(self):
        """
            Load all tasks from storage

            Returns:
                list of lib.models.task.Task objects - tasks

        """

        full_path = os.path.join(self.dir_path, JSONPath.TASKS)
        data = load_data(full_path)
        return data

    def save_tasks(self, tasks):
        """
            Save all tasks to storage

            Args:
                list of lib.models.task.Task objects - tasks

        """

        full_path = os.path.join(self.dir_path, JSONPath.TASKS)
        save_data(full_path, tasks)

    def load_task_actions(self):
        """
            Load all accesses from storage

            Returns:
                list of lib.models.access.Access objects - accesses

        """

        full_path = os.path.join(self.dir_path, JSONPath.TASK_ACTIONS)
        data = load_data(full_path)
        return data

    def save_task_actions(self, task_actions):
        """
            Save all accesses to storage

            Args:
                list of lib.models.access.Access objects - accesses

        """

        full_path = os.path.join(self.dir_path, JSONPath.TASK_ACTIONS)
        save_data(full_path, task_actions)

    def load_task_groups(self):
        """
            Load all task_groups from storage

            Returns:
                list of lib.models.group.TaskGroup objects - task_groups

        """

        full_path = os.path.join(self.dir_path, JSONPath.TASK_GROUPS)
        task_groups = load_data(full_path)
        return task_groups

    def save_task_groups(self, task_groups):
        """
            Save all task_groups to storage

            Args:
                list of lib.models.group.TaskGroup objects - task_groups

        """

        full_path = os.path.join(self.dir_path, JSONPath.TASK_GROUPS)
        save_data(full_path, task_groups)

    def load_groups(self):
        """
            Load all groups from storage

            Returns:
                list of lib.models.group.Group objects - groups

        """
        full_path = os.path.join(self.dir_path, JSONPath.GROUPS)
        groups = load_data(full_path)
        return groups

    def save_groups(self, groups):
        """
            Save all groups to storage

            Args:
                list of lib.models.group.Group objects - groups

        """

        full_path = os.path.join(self.dir_path, JSONPath.GROUPS)
        save_data(full_path, groups)

    def load_max_id(self):
        """
            Load all max_id data from storage

        """

        full_path = os.path.join(self.dir_path, JSONPath.MAX_ID)
        self.max_id = load_data(full_path)

    def save_max_id(self):
        """
            Save all max_id data to storage

        """

        full_path = os.path.join(self.dir_path, JSONPath.MAX_ID)
        save_data(full_path, self.max_id)

    def get_max_task_id(self):
        """
            Returns max id of created tasks

            Returns:
                 max id (int type)

        """

        return self.max_id['task']

    def get_max_group_id(self):
        """
            Returns max id of created groups

            Returns:
                 max id (int type)

        """

        return self.max_id['group']

    def get_max_plan_id(self):
        """
            Returns max plan created id

            Returns:
                max id (int type)

        """

        return self.max_id['plan']

    def load_plans(self):
        """
            Load all plans from storage

            Returns:
                list of lib.models.task_planner.TaskPlanner objects - plans

        """

        full_path = os.path.join(self.dir_path, JSONPath.PLANS)
        plans = load_data(full_path)
        return plans

    def save_plans(self, plans):
        """
            Save all plans to storage

            Args:
                list of lib.models.task_planner.TaskPlanner objects - plans

        """

        full_path = os.path.join(self.dir_path, JSONPath.PLANS)
        save_data(full_path, plans)

    def get_user_plans(self, user_login):
        """
            Returns all user created plans

            Args:
                user_login - user that requires plans

            Returns:
                list of lib.models.task_planner.TaskPlanner objects - user plans

        """

        plans = self.load_plans()
        user_plans = []
        for plan in plans:
            if plan.user_login == user_login:
                user_plans.append(plan)
        return user_plans

    def add_plan(self, plan):
        """
            Add plan to storage

            Args:
                plan - lib.models.task_planner.TaskPlanner object

        """

        plans = self.load_plans()
        plans.append(plan)
        self.max_id['plan'] += 1
        self.save_max_id()
        self.save_plans(plans)

    def update_plan(self, plan_id, plan):
        """
            Update plan information

            Args:
                plan_id - required plan id
                plan - lib.models.task_planner.TaskPlanner object - new plan

            Raises:
                PlanNotFound exception if plan not in storage

        """

        plans = self.load_plans()
        for i in range(len(plans)):
            if plans[i].id == plan_id:
                plans[i] = plan
                self.save_plans(plans)
                return
        raise PlanNotFound(plan_id)

    def delete_plan(self, plan_id):
        """
            Delete plan with required id

            Args:
                plan_id - required plan id

            Raises:
                PlanNotFound exception of plan not in storage

        """

        plans = self.load_plans()
        for i in range(len(plans)):
            if plans[i].id == plan_id:
                plans.remove(plans[i])
                self.save_plans(plans)
                return
        raise PlanNotFound(plan_id)

    def get_plan_by_id(self, plan_id):
        """
            Return plan with required id

            Args:
                plan_id - required plan id

            Returns:
                lib.models.task_planner.TaskPlanner object - required plan

            Raises:
                PlanNotFoundException if plan not in storage

        """

        plans = self.load_plans()
        for plan in plans:
            if plan.id == plan_id:
                return plan
        raise PlanNotFound(plan_id)


def load_data(path):
    """
        Load data from json file

        Args:
            path - path to json file

        Returns:
            loaded from file data
    """

    with open(path, 'r') as file:
        data = load(fp=file)
        return data


def save_data(path, data):
    """
        Save data to json file

        Args:
            path - path to json file
            data - saved data

    """

    with open(path, 'w') as file:
        dump(data, fp=file, indent=4)


def create_json_empty_file(path):
    """
        Creates json file with empty array

        Args:
            path - path to json file

    """

    with open(path, 'w') as file:
        file.write("[]")
