"""
    TaskTracker core lib

    Packages:
        models - application models
        storage - provides library storage logic and json storage object

    Modules:
        exceptions - library custom exceptions
        logger - library logger
        task_tracker - provides object with all library functions

    Usage:
        To use lib, type next commands:
            $ import os
            $ from lib.task_tracker import TaskTracker
            $ from lib.storage.storage import Storage
            $ storage = Storage(os.path.expanduser('~/.TaskTracker'))
            $ manager = TaskTracker(storage)

        You can use any directory started with ~/ instead of ~/.TaskTracker
        It will be directory with data files

        To see all library functions and description, type
            $ help(manager)

        Examples of using library functions:

            # We will create task 'walk a dog' with deadline on 5 p.m., 9th June, 2018 with 2-hour reminder
            # And add this task to 'family' group

            $ import datetime

            $ task_id = manager.create_task(user_login='Katskel', name='walk the dog')

            $ manager.edit_task(user_login='Katskel',
                                task_id=task_id,
                                new_end_date=datetime.datetime(2018, 6, 9, 17, 0),
                                new_reminder_time=datetime.timedelta(hours=2))

            $ manager.create_group(user_login='Katskel', group_name='family')
            $ manager.group_add_task(user_login='Katskel', group_name='family', task_id=task_id)

"""