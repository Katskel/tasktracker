"""
    Contains class with all library functions

    Classes:
        TaskTracker - class with library functions

"""

import os
import datetime
from lib.logger import get_logger
from lib.models.task_planner import TaskPlanner
from lib.models.group import (
    Group,
    TaskGroup,
)
from lib.models.task_action import (
    ActionType,
    TaskAction,
)
from lib.exceptions import (
    PermissionDenied,
    TaskActionNotFound,
)
from lib.models.task import (
    Task,
    TaskStatus,
    TaskPriority,
)


class TaskTracker:
    """
        Contains all library functions

        Values:
            storage - object with storage interface

    """

    def __init__(self, storage):
        self.storage = storage

    def create_task(self,
                    user_login,
                    name,
                    description='',
                    reminder_time=None,
                    end_date=None,
                    priority=0):
        """
            Create lib.models.task.Task object of user task
            Save this task to storage
            Create task action link between current user and created task

            Args:
                user_login - user that calls function (str)
                name - name of task (str)
                description - description of task (str)
                reminder_time - time in which the task will be shown to user as active (datetime.timedelta)
                end_date - last date to complete task, after task will be failed (datetime.datetime)
                priority - priority of task (int from 0 to 3)

        """

        task_id = self.storage.get_max_task_id() + 1
        task = Task(task_id=task_id,
                    name=name,
                    description=description,
                    reminder_time=reminder_time,
                    end_date=end_date,
                    priority=priority)

        task_action = TaskAction(task_id=task.id,
                                 user_login=user_login,
                                 action_type=ActionType.WRITE)

        self.storage.add_task(task)
        self.storage.add_task_action(task_action)


        get_logger().info('User {0} create task {1}'.format(user_login, task.id))
        return task.id

    def create_group(self, user_login, group_name):
        """
            Create lib.models.group.Group object of user group
            Save this group to storage

            Args:
                user_login - user that calls function
                group_name - name of created group

            Raises:
                 ValueError, if group_name already exists in user tasks

        """

        user_groups = self.storage.get_user_groups(user_login=user_login)
        if group_name in [group.name for group in user_groups]:
            raise ValueError('Such group already exists')

        group_id = self.storage.get_max_group_id() + 1
        group = Group(group_id=group_id,
                      name=group_name,
                      user_login=user_login)

        self.storage.add_group(group)

        get_logger().info('User {0} create group {1}'.format(user_login, group_name))
        return group.id

    def send_to_archive(self, user_login, task_id):
        """
            Mark task as archive task

            Args:
                user_login - user that calls function
                task_id - id of archived task

            Raises:
                PermissionDenied, if user has no action to edit task

        """

        task = self.storage.get_task_by_id(task_id=task_id)

        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if task_action != ActionType.WRITE:
            raise PermissionDenied("This user can not edit task")

        task.is_archive = True
        self.storage.update_task(task_id=task_id, task=task)

        get_logger().info('User {0} send to archive task {1}'.format(user_login, task_id))

    def perform_task(self, user_login, task_id):
        """
            Mark task as done

            Args:
                user_login - user that calls function
                task_id - id of marked task

            Raises:
                PermissionDenied, if user has no action to edit task

        """

        task = self.storage.get_task_by_id(task_id=task_id)

        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if task_action != ActionType.WRITE:
            raise PermissionDenied("This user can not edit task")

        task.status = TaskStatus.DONE
        self.storage.update_task(task_id=task_id, task=task)

        get_logger().info('User {0} perform task {1}'.format(user_login, task_id))

    def start_task(self, user_login, task_id):
        """
            Mark task as started (active)

            Args:
                user_login - user that calls function
                task_id - id of marked task

            Raises:
                PermissionDenied, if user has no action to edit task

        """

        task = self.storage.get_task_by_id(task_id=task_id)

        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if task_action != ActionType.WRITE:
            raise PermissionDenied("This user can not edit task")

        task.status = TaskStatus.IN_PROCESS
        self.storage.update_task(task_id=task_id, task=task)

        get_logger().info('User {0} starts task {1}'.format(user_login, task_id))

    def stop_task(self, user_login, task_id):
        """
            Mark task as not started

            Args:
                user_login - user that calls function
                task_id - id of marked task

            Raises:
                PermissionDenied, if user has no action to edit task

        """

        task = self.storage.get_task_by_id(task_id=task_id)

        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if task_action != ActionType.WRITE:
            raise PermissionDenied("This user can not edit task")

        task.status = TaskStatus.NOT_STARTED
        self.storage.update_task(task_id=task_id, task=task)

        get_logger().info('User {0} stops task {1}'.format(user_login, task_id))

    def fail_task(self, user_login, task_id):
        """
            Mark task as failed

            Args:
                user_login - user that calls function
                task_id - id of marked task

            Raises:
                PermissionDenied, if user has no action to edit task

        """

        task = self.storage.get_task_by_id(task_id=task_id)

        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if task_action != ActionType.WRITE:
            raise PermissionDenied("This user can not edit task")

        task.status = TaskStatus.FAILED
        self.storage.update_task(task_id=task_id, task=task)

        get_logger().info('User {0} fail task {1}'.format(user_login, task_id))

    def get_tasks_tree(self, user_login, task_id):
        task = self.storage.get_task_by_id(task_id=task_id)
        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if task_action is None:
            raise PermissionDenied("This user can not see task")

        tasks_tree = dict()
        tasks_tree[0] = task

        subtasks = self.get_task_subtasks(task_id=task_id)
        i = 1

        while len(subtasks) != 0:
            tasks_tree[i] = subtasks
            subtasks = []
            for item in tasks_tree[i]:
                subtasks.append(self.get_task_subtasks(item.id))
            i += 1
        return tasks_tree

    def edit_task(self, user_login, task_id, new_name=False, new_description=False, new_reminder_time=False,
                  new_end_date=False, new_priority=False):
        task = self.storage.get_task_by_id(task_id=task_id)

        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if task_action != ActionType.WRITE:
            raise PermissionDenied("This user can not edit task")

        if new_name is not False:
            if new_name == '':
                raise ValueError("Name should be non-empty")
            task.name = new_name
            get_logger().info('User {0} change task {1} name to {2}'.format(user_login, task_id, new_name))

        if new_description is not False:
            task.description = new_description
            get_logger().info('User {0} change task {1} description to {2}'.format(user_login, task_id, new_description))

        if new_reminder_time is not False:
            if new_reminder_time is not None and new_reminder_time <= datetime.timedelta():
                raise ValueError('Reminder time should be positive')
            task.reminder_time = new_reminder_time
            get_logger().info('User {0} change task {1} reminder time to {2}'.format(
                user_login, task_id, str(new_reminder_time)))

        if new_end_date is not False:
            task.end_date = new_end_date
            get_logger().info('User {0} change task {1} end date to {2}'.format(user_login, task_id, str(new_end_date)))

        if new_priority is not False:
            task.priority = TaskPriority(new_priority)
            self.storage.update_task(task_id=task_id, task=task)
            get_logger().info('User {0} change task {1} priority to {2}'.format(
                user_login, task_id, TaskPriority(new_priority).name))

        self.storage.update_task(task_id=task_id, task=task)

    def delete_task(self, user_login, task_id):
        """
            Delete task and all subtask links

            Args:
                user_login - user that calls function
                task_id - id of deleted task

            Raises:
                PermissionDenied, if user has no action to edit task

        """

        task = self.storage.get_task_by_id(task_id=task_id)

        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if task_action != ActionType.WRITE:
            raise PermissionDenied("This user can not edit task")

        subtasks = self.get_task_subtasks(task_id=task.id)
        for subtask in subtasks:
            subtask.head_task_id = None
            self.storage.update_task(task_id=subtask.id, task=subtask)

        self.storage.delete_task(task_id=task.id)

        get_logger().info('User {0} delete task {1}'.format(user_login, task.id))

    def group_add_task(self, user_login, group_name, task_id):
        """
            Create link between task and group (group contains a task)

            Args:
                user_login - user that calls function
                group_name - name of group that containes task
                task_id - contained task id

            Raises:
                PermissionDenied, if user has no action to edit task
                ValueError, if task already in group

        """
        task = self.storage.get_task_by_id(task_id=task_id)

        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if task_action != ActionType.WRITE:
            raise PermissionDenied("This user can not edit task")

        group = self.storage.get_group_by_name(group_name=group_name, user_login=user_login)
        group_tasks = self.storage.get_group_tasks(user_login=user_login, group_name=group_name)

        tasks_id = [item.id for item in group_tasks]
        if task_id in tasks_id:
            raise ValueError('Task already in group')

        task_group = TaskGroup(task_id=task_id, group_id=group.id)
        self.storage.add_task_group(task_group=task_group)

        get_logger().info('User {0} add task {1} to group {2}'.format(user_login, task_id, group.id))

    def group_delete_task(self, user_login, group_name, task_id):
        """
            Delete link between task and group (delete task from user group)

            Args:
                user_login - user that calls function
                group_name - name of group
                task_id - deleted task id

            Raises:
                PermissionDenied, if user has no action to edit task
                ValueError, if task not in group

        """
        task = self.storage.get_task_by_id(task_id=task_id)

        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if task_action != ActionType.WRITE:
            raise PermissionDenied("This user can not edit task")

        group = self.storage.get_group_by_name(group_name=group_name, user_login=user_login)
        group_tasks = self.storage.get_group_tasks(user_login=user_login, group_name=group_name)

        tasks_id = [item.id for item in group_tasks]
        if task_id not in tasks_id:
            raise ValueError('No such task in group')

        self.storage.delete_task_group(task_id=task_id, group_id=group.id)

        get_logger().info('User {0} delete task {1} from group {2}'.format(user_login, task_id, group.id))

    def group_edit_name(self, user_login, group_name, new_name):
        """
            Change user group name

            Args:
                user_login - user that calls function
                group_name - name of user group
                new_name - new name for group

            Raises:
                ValueError, if new name is empty or already contained in user groups names

        """

        group = self.storage.get_group_by_name(group_name=group_name, user_login=user_login)
        user_groups = self.storage.get_user_groups(user_login=user_login)

        if new_name in [group.name for group in user_groups]:
            raise ValueError('Such group already exists')

        if new_name == '':
            raise ValueError('Group name must be non-empty')

        group.name = new_name
        self.storage.update_group(group_id=group.id, group=group)

        get_logger().info('User {0} change group {1} name to {2}'.format(user_login, group.id, new_name))

    def get_group_tasks(self, user_login, group_name):
        """
            Returns all user group tasks

            Args:
                user_login - user that calls function
                group_name - name of user group

            Returns:
                list of lib.models.task.Task objects - group tasks

        """

        return self.storage.get_group_tasks(user_login=user_login, group_name=group_name)

    def get_group(self, user_login, group_name):
        """
            Returns user group

            Args:
                user_login - user that calls function
                group_name - required user group name

            Returns:
                lib.models.group.Group object - required user group

        """

        return self.storage.get_group_by_name(group_name=group_name, user_login=user_login)

    def get_groups(self, user_login):
        """
            Returns all user groups

            Args:
                user_login - user that calls function

            Returns:
                list of lib.models.group.Group objects - all user groups

        """

        return self.storage.get_user_groups(user_login=user_login)

    def get_task_action(self, user_login, task_id):
        """
            Returns task action link between user and task

            Args:
                user_login - user that calls function
                task_id - required task id

            Returns:
                lib.models.task_action.TaskAction object - type of user action with task

        """

        return self.storage.check_task_action(task_id=task_id, user_login=user_login)

    def get_task(self, user_login, task_id):
        """
            Returns required task

            Args:
                user_login - user that calls function
                task_id - required task id

            Returns:
                lib.models.task.Task object - required task

            Raises:
                PermissionDenied, if user can not read the task

        """

        task = self.storage.get_task_by_id(task_id=task_id)

        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if task_action is None:
            raise PermissionDenied("This user can not see this task")

        return task

    def get_tasks(self, user_login, name=False, description=False, end_date=False, status=False, priority=False, is_archive=None):
        """
            Returns all user tasks that satisfy condition

            Args:
                user_login - user that calls function
                filter: lambda function, condition for tasks (No condition by default)

            Returns:
                list of lib.models.task.Task objects - user tasks that satisfy the condition

        """

        return self.storage.get_user_tasks(user_login=user_login,
                                           name=name,
                                           description=description,
                                           end_date=end_date,
                                           status=status,
                                           priority=priority,
                                           is_archive=is_archive)

    def get_task_subtasks(self, task_id):
        """
            Returns all task subtasks

            Args:
                task_id - id of required task

            Returns:
                list of lib.models.task.Task objects - task subtasks

        """

        return self.storage.get_task_subtasks(task_id=task_id)

    def delete_group(self, user_login, group_name):
        """
            Delete user group

            Args:
                user_login - user that calls function
                group_name - user deleted group name

        """

        group = self.storage.get_group_by_name(group_name=group_name, user_login=user_login)
        group_tasks = self.storage.get_group_tasks(user_login=user_login, group_name=group_name)

        for task in group_tasks:
            self.storage.delete_task_group(task_id=task.id, group_id=group.id)

        self.storage.delete_group(group_name=group_name, user_login=user_login)

        get_logger().info('User {0} delete group {1}'.format(user_login, group_name))

    def add_subtask(self, user_login, task_id, subtask_id):
        """
            Add subtask to task

            Args:
                user_login - user that calls function
                task_id - id of parent task
                subtask_id - id of subtask

            Raises:
                PermissionDenied, if user can not edit task or when created cyclic links

        """
        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        subtask_action = self.storage.check_task_action(task_id=subtask_id, user_login=user_login)

        if task_action != ActionType.WRITE or subtask_action != ActionType.WRITE:
            raise PermissionDenied('Can not edit task'
                                   '')
        if task_id == subtask_id:
            raise PermissionDenied('Cyclic subtasks')

        task = self.storage.get_task_by_id(task_id=task_id)
        while task.head_task_id is not None:
            if task.head_task_id == subtask_id:
                raise PermissionDenied('Cyclic subtasks')
            task = self.storage.get_task_by_id(task_id=task.head_task_id)

        subtask = self.storage.get_task_by_id(task_id=subtask_id)
        subtask.head_task_id = task_id
        self.storage.update_task(task_id=subtask_id, task=subtask)
        get_logger().info('Subtask {0} was added to task {1}'.format(subtask_id, task_id))

    def delete_subtask(self, user_login, task_id, subtask_id):
        """
            Delete subtask from task

            Args:
                user_login - user that calls function
                task_id - parent task id
                subtask_id - if of deleted subtask

            Raises:
                PermissionDenied, when user can not edit tasks or parent task has no this subtask

        """

        task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        subtask_action = self.storage.check_task_action(task_id=subtask_id, user_login=user_login)

        if task_action != ActionType.WRITE or subtask_action != ActionType.WRITE:
            raise PermissionDenied('Can not edit task')

        task = self.storage.get_task_by_id(task_id=task_id)
        subtask = self.storage.get_task_by_id(task_id=subtask_id)

        if subtask.head_task_id != task.id:
            raise PermissionDenied('Task {0} has no subtask {1}'.format(task.id, subtask_id))

        subtask.head_task_id = None
        self.storage.update_task(task_id=subtask_id, task=subtask)

        get_logger().info('Subtask {0} was deleted from task {1}'.format(subtask_id, task.id))

    def task_allow_read(self, user_login, task_id, allow_user_login):
        """
            Allow user to read other task

            Args:
                user_login - user that calls function
                task_id - id of allowed task
                allow_user_login - user who can read task

            Raises:
                PermissionDenied, if user that allows has no action to edit task

        """

        user_task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if user_task_action != ActionType.WRITE:
            raise PermissionDenied("This user can not edit task")

        allow_task_action = self.storage.check_task_action(task_id=task_id, user_login=allow_user_login)
        if allow_task_action is not None:
            self.storage.delete_task_action(task_id=task_id, user_login=allow_user_login)

        allow_task_action = TaskAction(task_id=task_id, user_login=allow_user_login, action_type=ActionType.READ)
        self.storage.add_task_action(task_action=allow_task_action)

        get_logger().info(
            'User {0} allows to read task {1} for user {2}'.format(user_login, task_id, allow_user_login))

    def task_allow_write(self, user_login, task_id, allow_user_login):
        """
            Allow user to write other task

            Args:
                user_login - user that calls function
                task_id - id of allowed task
                allow_user_login - user who can write task

            Raises:
                PermissionDenied, if user that allows has no action to edit task

        """

        user_task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if user_task_action != ActionType.WRITE:
            raise PermissionDenied("This user can not edit task")

        allow_task_action = self.storage.check_task_action(task_id=task_id, user_login=allow_user_login)
        if allow_task_action is not None:
            self.storage.delete_task_action(task_id=task_id, user_login=allow_user_login)

        allow_task_action = TaskAction(task_id=task_id, user_login=allow_user_login, action_type=ActionType.WRITE)
        self.storage.add_task_action(task_action=allow_task_action)

        get_logger().info(
            'User {0} allows to write task {1} for user {2}'.format(user_login, task_id, allow_user_login))

    def task_disallow(self, user_login, task_id, disallow_user_login):
        """
            Disallow user to read other task

            Args:
                user_login - user that calls function
                task_id - id of disallowed task
                disallow_user_login - user that lose action

            Raises:
                PermissionDenied, if user that disallows has no action to edit task
                TaskActionNotFound, if user allow action was empty (nothing to delete)

        """

        user_task_action = self.storage.check_task_action(task_id=task_id, user_login=user_login)
        if user_task_action != ActionType.WRITE:
            raise PermissionDenied("This user can not edit task")

        allow_task_action = self.storage.check_task_action(task_id=task_id, user_login=disallow_user_login)
        if allow_task_action is None:
            raise TaskActionNotFound(task_id=task_id, user_login=disallow_user_login)

        self.storage.delete_task_action(task_id=task_id, user_login=disallow_user_login)

        get_logger().info(
            'User {0} disallows to see task {1} for user {2}'.format(user_login, task_id, disallow_user_login))

    def update_tasks(self, user_login):
        """
            Update all user tasks statuses

            Args:
                user_login - user that calls function

        """

        tasks = self.get_tasks(user_login=user_login)

        for task in tasks:
            if task.end_date is None:
                continue
            if task.status == TaskStatus.IN_PROCESS and datetime.datetime.now() > task.end_date:
                self.fail_task(user_login=user_login, task_id=task.id)
                continue
            if task.reminder_time is None:
                if task.status == TaskStatus.NOT_STARTED:
                    self.start_task(user_login=user_login, task_id=task.id)
            else:
                if (task.status == TaskStatus.NOT_STARTED and
                        task.reminder_time > task.end_date - datetime.datetime.now()):
                    self.start_task(user_login=user_login, task_id=task.id)
                if (task.status == TaskStatus.IN_PROCESS and
                        task.reminder_time < task.end_date - datetime.datetime.now()):
                    self.stop_task(user_login=user_login, task_id=task.id)

    def create_plan(self,
                    user_login,
                    task_name,
                    end_time,
                    repeat_days=None,
                    repeat_count=0,
                    repeat_time_delta=None,
                    reminder_time=None,
                    task_description='',
                    task_priority=0):
        """
            Create lib.models.task_planner.TaskPlanner object of user plan
            Save this plan to storage

            Args:
                user_login - user that calls functions
                task_name - name for task created with plan
                end_time - last date to complete task created with plan, after task will be failed
                repeat_days - the days in which plan will create tasks
                repeat_count - number of times that plan will create task
                repeat_time_delta - plan will create task every time interval (higher priority that repeat_days)
                reminder_time - time in which the created task will be shown to user as active
                task_description - description of task created with plan
                task_priority - priority of task created with plan

        """

        plan_id = self.storage.get_max_plan_id() + 1
        plan = TaskPlanner(plan_id=plan_id,
                           task_name=task_name,
                           end_date=end_time,
                           user_login=user_login,
                           repeat_days=repeat_days,
                           repeat_count=repeat_count,
                           repeat_time_delta=repeat_time_delta,
                           reminder_time=reminder_time,
                           task_description=task_description,
                           task_priority=TaskPriority(task_priority))

        self.storage.add_plan(plan=plan)

        get_logger().info('User {0} create plan {1}'.format(user_login, plan.id))

        return plan.id

    def get_plans(self, user_login):
        """
            Returns all user plans

            Args:
                user_login - user that calls function

            Returns:
                list of lib.models.task_planner.TaskPlanner objects - user plans

        """

        return self.storage.get_user_plans(user_login=user_login)

    def update_plans(self, user_login):
        """
            Update all plans and create new plan tasks if it's time for that:

            Args:
                user_login - user that calls function

        """

        plans = self.get_plans(user_login=user_login)

        for plan in plans:
            if plan.end_date is None:
                continue
            if plan.last_added is not None and plan.last_added > datetime.datetime.now():
                continue
            if plan.repeat_count is not None and plan.repeat_count <= 0:
                continue
            if plan.repeat_count is None and plan.repeat_days is None and plan.repeat_time_delta is None:
                continue
            if plan.last_added is None:
                self.create_task(user_login=user_login,
                                 name=plan.task_name,
                                 description=plan.task_description,
                                 reminder_time=plan.reminder_time,
                                 end_date=plan.end_date,
                                 priority=plan.task_priority)
                plan.last_added = plan.end_date
                if plan.repeat_count is not None:
                    plan.repeat_count -= 1
            else:
                next_date = plan.last_added
                if plan.repeat_time_delta is not None:
                    next_date += plan.repeat_time_delta
                    while next_date < datetime.datetime.now():
                        next_date += plan.repeat_time_delta
                elif plan.repeat_days is not None:
                    next_date += datetime.timedelta(days=1)
                    while next_date < datetime.datetime.now() or next_date.weekday() not in plan.repeat_days:
                        next_date += datetime.timedelta(days=1)
                else:
                    continue
                self.create_task(user_login=user_login,
                                 name=plan.task_name,
                                 description=plan.task_description,
                                 reminder_time=plan.reminder_time,
                                 end_date=next_date,
                                 priority=plan.task_priority)
                plan.last_added = next_date
                if plan.repeat_count is not None:
                    plan.repeat_count -= 1
            self.storage.update_plan(plan_id=plan.id, plan=plan)

    def delete_plan(self, user_login, plan_id):
        """
            Delete user plan

            Args:
                user_login - user that calls function
                plan_id - user deleted plan id

            Raises:
                PermissionDenied if user is not plan's owner

        """

        plan = self.storage.get_plan_by_id(plan_id=plan_id)
        if plan.user_login != user_login:
            raise PermissionDenied('User can not edit plan')

        self.storage.delete_plan(plan_id=plan_id)

        get_logger().info('User {0} delete plan {1}'.format(user_login, plan_id))

    def edit_plan(self, user_login, plan_id, new_name=False, new_description=False, new_reminder_time=False,
                  new_priority=False, new_repeat=False, new_week=False, new_delta=False):

        plan = self.storage.get_plan_by_id(plan_id=plan_id)
        if plan.user_login != user_login:
            raise PermissionDenied("This user can not edit plan")

        if new_name is not False:
            if new_name == '':
                raise ValueError("Name should be non-empty")
            plan.task_name = new_name
            get_logger().info('Plan {0} name changed to {1}'.format(plan_id, new_name))

        if new_description is not False:
            plan.task_description = new_description
            get_logger().info('Plan {0} description changed to {1}'.format(plan_id, new_description))

        if new_reminder_time is not False:
            if new_reminder_time is not None and new_reminder_time <= datetime.timedelta():
                raise ValueError('Reminder time should be positive')
            plan.reminder_time = new_reminder_time
            get_logger().info('Plan {0} reminder time changed to {1}'.format(plan_id, str(new_reminder_time)))

        if new_priority is not False:
            plan.task_priority = TaskPriority(new_priority)
            get_logger().info('Plan {0} priority changed to {1}'.format(plan_id, TaskPriority(new_priority).name))

        if new_repeat is not False:
            if new_repeat <= 0:
                raise ValueError('Repeat times should be positive')
            plan.repeat_count = new_repeat
            get_logger().info('Plan {0} repeat count time changed to {1}'.format(plan_id, str(new_repeat)))

        if new_week is not False:
            if len(new_week) == 0:
                raise ValueError('Week should by non-empty')
            plan.repeat_days = new_week
            get_logger().info('Plan {0} repeat week days time changed to {1}'.format(plan_id, str(new_week)))

        if new_delta is not False:
            if new_delta is not None and new_delta < datetime.timedelta():
                raise ValueError('Time delta should be positive')
            plan.repeat_time_delta = new_delta
            get_logger().info('Plan {0} delta time changed to {1}'.format(plan_id, str(new_delta)))

        self.storage.update_plan(plan_id=plan_id, plan=plan)
