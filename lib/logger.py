"""
    Provides library logger functions

"""

import logging


def get_logger():
    """
        Returns library logger
    """

    return logging.getLogger('TaskTracker')


def enable_logger():
    """
        Enable library logger
    """

    get_logger().disabled = False


def disable_logger():
    """
        Disable library logger
    """

    get_logger().disabled = True
