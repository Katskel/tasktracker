"""
    Functions to work with application logger

"""

import logging
import os
from console.config_worker import ConfigWorker
from lib.logger import (
    get_logger,
    enable_logger,
    disable_logger,
)


def init_logger():
    """
        Init application logger, using app config properties

    """

    config_worker = ConfigWorker()
    logger = get_logger()
    data_path = os.path.expanduser(config_worker.get_data_path())
    file_handler = logging.FileHandler(os.path.join(data_path, 'log'))
    logger.addHandler(file_handler)
    logger.setLevel(logging.DEBUG)
    logging_mode = config_worker.get_logging()
    if logging_mode == 'on':
        enable_logger()
    else:
        disable_logger()
