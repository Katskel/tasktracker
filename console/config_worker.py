"""
    Work with console app configuration

    Classes:
        ConfigFieldsNames - consts names for configuration
        ConfigWorker - class that works with configuration

    Functions:
        main() - entry point for configuration changing

"""


import configparser
import os
import argparse
from lib.logger import get_logger
from lib.exceptions import (
    InvalidConfig,
    ConfigNotFound,
)


class ConfigFieldsNames:
    """
        Const names for ConfigWorker

    """

    SECTION = 'Common'
    STORAGE_PATH = 'path'
    USER = 'user'
    LOGGING_ON = 'on'
    LOGGING_OFF = 'off'
    LOGGING_MODE = 'logging'
    DEFAULT_CONFIG_DIR = '~/.TaskTracker'
    DEFAULT_CONFIG_TEST_DIR = '~/.TaskTracker_test'
    CONFIG_NAME = 'config.cfg'


class ConfigWorker:
    """
        Class that works with configuration

        Values:
            config_path - path for config file
            config - object that works with config file

    """

    def __init__(self):
        self.config_path = os.path.expanduser(os.path.join(ConfigFieldsNames.DEFAULT_CONFIG_DIR, ConfigFieldsNames.CONFIG_NAME))
        self.config = configparser.RawConfigParser()

    def show_config_options(self):
        """
            Shows config options to console

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound()
        self.config.read(self.config_path)
        for section in self.config.sections():
            print("Section:", section)
            for item in self.config.items(section):
                print(": ".join(item))

    def validate_config(self):
        """
            Check config file for correct data
            Correct data: non-empty user name, logging mode (on/off),
                directory starts with "~/"

            Raises:
                InvalidConfig exception if some data errors in config
        """

        if not os.path.exists(self.config_path):
            raise ConfigNotFound()
        self.config.read(self.config_path)
        user = self.config[ConfigFieldsNames.SECTION][ConfigFieldsNames.USER]
        if user == '':
            raise InvalidConfig('No user')
        path = str(self.config[ConfigFieldsNames.SECTION][ConfigFieldsNames.STORAGE_PATH])
        if not path.startswith('~/'):
            raise InvalidConfig('Incorrect directory path')
        logging = self.config[ConfigFieldsNames.SECTION][ConfigFieldsNames.LOGGING_MODE]
        if logging != ConfigFieldsNames.LOGGING_ON and logging != ConfigFieldsNames.LOGGING_OFF:
            raise InvalidConfig('Incorrect logging mode')

    def set_default_config(self):
        """
            Set default config properties:
                empty user, logging mode = off, directory from ConfigFieldsNames class
            Also creates config file if not exists


        """
        if not self.config.has_section(ConfigFieldsNames.SECTION):
            self.config.add_section(ConfigFieldsNames.SECTION)
        self.config[ConfigFieldsNames.SECTION][ConfigFieldsNames.USER] = ''
        self.config[ConfigFieldsNames.SECTION][ConfigFieldsNames.STORAGE_PATH] = ConfigFieldsNames.DEFAULT_CONFIG_DIR
        self.config[ConfigFieldsNames.SECTION][ConfigFieldsNames.LOGGING_MODE] = ConfigFieldsNames.LOGGING_ON
        if not os.path.exists(os.path.expanduser(ConfigFieldsNames.DEFAULT_CONFIG_DIR)):
            os.mkdir(os.path.expanduser(ConfigFieldsNames.DEFAULT_CONFIG_DIR))
        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def get_user(self):
        """
            Returns user name from config file

            Returns:
                user name (str type)

            Raises:
                ConfigNotFound exception, if config file not exists

        """

        if not os.path.exists(self.config_path):
            raise ConfigNotFound()
        self.config.read(self.config_path)
        return self.config[ConfigFieldsNames.SECTION][ConfigFieldsNames.USER]

    def set_user(self, user):
        """
            Set user name to config file

            Args:
                user - user name (str)

            Raises:
                ConfigNotFound, if config file not exists

        """

        if not os.path.exists(self.config_path):
            raise ConfigNotFound()
        self.config.read(self.config_path)
        self.config[ConfigFieldsNames.SECTION][ConfigFieldsNames.USER] = user
        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def get_data_path(self):
        """
            Returns data directory path from config file

            Returns:
                data path (str type)

            Raises:
                ConfigNotFound exception, if config file not exists

        """

        if not os.path.exists(self.config_path):
            raise ConfigNotFound()
        self.config.read(self.config_path)
        return self.config[ConfigFieldsNames.SECTION][ConfigFieldsNames.STORAGE_PATH]

    def set_data_path(self, path):
        """
            Set data directory path to config file

            Args:
                path - data directory path (str)

            Raises:
                ConfigNotFound, if config file not exists

        """

        if not os.path.exists(self.config_path):
            raise ConfigNotFound()
        self.config.read(self.config_path)
        self.config[ConfigFieldsNames.SECTION][ConfigFieldsNames.STORAGE_PATH] = path
        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def get_logging(self):
        """
            Returns logging mode from config file

            Returns:
                logging mode (str 'on'/'off' only)

            Raises:
                ConfigNotFound exception, if config file not exists

        """

        if not os.path.exists(self.config_path):
            raise ConfigNotFound()
        self.config.read(self.config_path)
        return self.config[ConfigFieldsNames.SECTION][ConfigFieldsNames.LOGGING_MODE]

    def set_logging(self, logging):
        """
            Set logging mode to config file

            Args:
                logging (str 'on'/'off' only)

            Raises:
                ConfigNotFound, if config file not exists

        """

        if not os.path.exists(self.config_path):
            raise ConfigNotFound()
        self.config.read(self.config_path)
        self.config[ConfigFieldsNames.SECTION][ConfigFieldsNames.LOGGING_MODE] = logging.lower()
        with open(self.config_path, 'w') as file:
            self.config.write(file)


def main():
    """
        Entry point for configuration changing

    """

    parser = argparse.ArgumentParser(description='Work with config settings')
    parser.add_argument('-s', '--show', action='store_true', help='Show config options')
    parser.add_argument('-d', '--default', action='store_true', help='Create default config')
    parser.add_argument('-p', '--path', nargs=1, help='Path for data')
    parser.add_argument('-u', '--user', nargs=1, help='Set user')
    parser.add_argument('-log', '--logger',
                        choices=[ConfigFieldsNames.LOGGING_ON, ConfigFieldsNames.LOGGING_OFF],
                        help='Set logger mode')
    try:
        config_worker = ConfigWorker()
        args = parser.parse_args()

        if args.show:
            config_worker.show_config_options()

        if args.default:
            config_worker.set_default_config()

        if args.path:
            config_worker.set_data_path(path=args.path[0])

        if args.user:
            config_worker.set_user(user=args.user[0])

        if args.logger:
            config_worker.set_logging(logging=args.logger)
    except Exception as ex:
        print(ex)
        get_logger().error(str(ex))
