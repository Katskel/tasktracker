"""
    Contains main function

"""


import os
from lib.task_tracker import TaskTracker
from console.config_worker import ConfigWorker
from console.logger_worker import init_logger
from lib.logger import get_logger
from lib.storage.storage import Storage
from console.parsers.argument_parser import ArgumentParser
from console.parsers import (
    group_parser,
    groups_parser,
    task_parser,
    tasks_parser,
    show_parser,
    plans_parser,
    plan_parser,
)


def main():
    """
        Main function, entry point for application
        Load config, init directories, init loggers
        Parse console input args and send to lib

    """

    config_worker = ConfigWorker()

    try:
        config_worker.validate_config()
        init_logger()
        user = config_worker.get_user()
        data_path = config_worker.get_data_path()

        storage = Storage(os.path.expanduser(data_path))
        manager = TaskTracker(storage)
        manager.update_tasks(user_login=user)
        manager.update_plans(user_login=user)

        parser = ArgumentParser(description='TaskTracker')
        parser.set_defaults(which='main')
        subparsers = parser.add_subparsers()

        group_parsers = group_parser.init_parser(subparsers)
        groups_parsers = groups_parser.init_parser(subparsers)
        task_parsers = task_parser.init_parser(subparsers)
        tasks_parsers = tasks_parser.init_parser(subparsers)
        show_parsers = show_parser.init_parser(subparsers)
        plans_parsers = plans_parser.init_parser(subparsers)
        plan_parsers = plan_parser.init_parser(subparsers)

        args = parser.parse_args()

        group_parser.parse_args(parser=group_parsers,
                                args=args,
                                manager=manager,
                                user=user)

        groups_parser.parse_args(parser=groups_parsers,
                                 args=args,
                                 manager=manager,
                                 user=user)

        task_parser.parse_args(parser=task_parsers['task'],
                               remind_parser=task_parsers['remind'],
                               allow_parser=task_parsers['allow'],
                               args=args,
                               manager=manager,
                               user=user)

        tasks_parser.parse_args(parser=tasks_parsers,
                                args=args,
                                manager=manager,
                                user=user)

        show_parser.parse_args(parser=show_parsers,
                               args=args, manager=manager,
                               user=user)

        plans_parser.parse_args(parser=plans_parsers['plan'],
                                add_parser=plans_parsers['add'],
                                args=args,
                                manager=manager,
                                user=user)

        plan_parser.parse_args(parser=plan_parsers['plan'],
                               remind_parser=plan_parsers['reminder'],
                               args=args,
                               manager=manager,
                               user=user)

        if args.which == 'main':
            parser.error('no arguments received')

    except Exception as ex:
        print(ex)
        get_logger().error(str(ex))


if __name__ == '__main__':
    main()
