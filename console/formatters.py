"""
    Contains functions that format data for console output

"""


from termcolor import colored
from lib.models.task import TaskStatus


def task_short_format(task):
    """
        Returns task information:
            id,
            name,
            end_date,
            priority,

        Args:
            task - required task

        Returns:
            task info (str type)
    """

    task_repr = '{id}:\t{name}\t{date}\tPriority:{priority}'.format(
        id=task.id,
        name=task.name,
        date=task.end_date,
        priority=task.priority.name)

    color = 'magenta'
    if task.status == TaskStatus.DONE:
        color = 'green'
    elif task.status == TaskStatus.FAILED:
        color = 'red'
    elif task.status == TaskStatus.IN_PROCESS:
        color = 'white'
    elif task.is_archive is True:
        color = 'blue'
    return colored(task_repr, color)


def task_full_format(task, subtasks):
    """
        Returns task information:
            id,
            name,
            description,
            end_date,
            status,
            priority,
            is_archive,
            head_task_id,
            reminder,
            information about subtasks

        Args:
            task - required task
            subtasks - subtasks of required task

        Returns:
            task info (str type)

    """

    id = 'Id: {0}'.format(task.id)
    name = 'Name: {0}'.format(task.name)
    description = 'Description: {0}'.format(task.description)
    end_date = 'End date: {0}'.format(str(task.end_date))
    status = 'Status: {0}'.format(task.status.name)
    priority = 'Priority: {0}'.format(task.priority.name)
    is_archive = 'Archive: {0}'.format(task.is_archive)
    head_task_id = 'Head task id: {0}'.format(task.head_task_id)
    reminder = 'Reminder: {0}'.format(str(task.reminder_time))
    task_full = '\n'.join((
        id,
        name,
        description,
        end_date,
        status,
        priority,
        is_archive,
        head_task_id,
        reminder,
    ))
    subtask_format = ['No subtasks'] if len(subtasks) == 0 else ['Subtasks:']
    for subtask in subtasks:
        subtask_format.append(task_short_format(subtask))
    subtasks_full = '\n'.join(subtask_format)

    full_format = '\n'.join((
        task_full,
        subtasks_full,
    ))
    return full_format


def group_short_format(group):
    """
        Returns group information:
            id,
            name

        Args:
            group - required group

        Returns:
            group info (str type)

    """

    group_repr = '{id}:\t{name}'.format(id=group.id, name=group.name)
    return group_repr


def group_full_format(group, tasks):
    """
        Returns group information:
            id,
            name,
            information about contained tasks

        Args:
            group - reqiured group
            tasks - tasks contained in group

        Returns:
            group info (str type)

    """

    group_short = group_short_format(group)

    tasks_repr = ['No tasks'] if len(tasks) == 0 else ['Tasks:']
    for task in tasks:
        tasks_repr.append(task_short_format(task))
    tasks_repr.insert(0, group_short)

    full_format = '\n'.join(tasks_repr)
    return full_format


def plan_short_format(plan):
    """
        Returns plan information:
            id,
            name,
            week_days,
            period,
            repeat_count

        Args:
            plan - required plan

        Returns:
            plan info (str type)

    """

    plan_repr = '{id}:\t{name}\tDays:{days}\tPeriod:{period}\tRepeat:{repeat}'.format(
        id=plan.id,
        name=plan.task_name,
        days=plan.repeat_days,
        period=plan.repeat_time_delta,
        repeat=plan.repeat_count)
    return plan_repr
