"""
    TaskTracker console application package.

    Packages:
        lib - core console lib
        parsers - command parsers for user console application

    Modules:
        config_worker - work with configuration, entry point for configuration changing
        logger_worker - function that creates logger and configuration
        console - main function, entry point for application
        formatters - contains functions that format data for console output

"""