"""
    Parser for editing single task
"""

import datetime
from console.formatters import task_full_format


def init_parser(subparsers):
    task_parser = subparsers.add_parser('task', help='single task editing')
    task_parser.add_argument('task_id', type=int, help='task id')
    task_parser.add_argument('-i', '--info', action='store_true', help='show full task info')
    task_parser.add_argument('-n', '--name', dest='new_task_name', help='change task name')
    task_parser.add_argument('-d', '--desc', dest='new_task_desc', help='change task description')
    task_parser.add_argument('-p', '--priority', dest='new_task_pr', choices=['0', '1', '2', '3'],
                             help='change task priority')

    task_parser.add_argument('-ed', '--edate', dest='new_task_ed',
                             type=lambda d: datetime.datetime.strptime(d, '%d.%m.%Y %H:%M'),
                             help='change task end date (in DD.MM.YYYY H:M format)')
    task_parser.add_argument('-as', '--addsub', type=int, dest='add_subtask_id',
                             help='add subtask to task (subtask id)')
    task_parser.add_argument('-rs', '--rmsub', type=int, dest='remove_subtask_id',
                             help='remove subtask from task (subtask id)')
    task_parser.set_defaults(which='task')
    task_subparser = task_parser.add_subparsers()

    reminder_parser = task_subparser.add_parser('remind', help='set time for reminder')
    reminder_parser.add_argument('-m', '--minutes', nargs=1, type=int, help='remind m minutes before')
    reminder_parser.add_argument('-hr', '--hours', nargs=1, type=int, help='remind h hours before')
    reminder_parser.add_argument('-d', '--days', nargs=1, type=int, help='remind d days before')
    reminder_parser.add_argument('-rm', '--remove', action='store_true', help='delete reminder')
    reminder_parser.set_defaults(which='task_reminder')

    allow_parser = task_subparser.add_parser('allow', help='allow user to do smth with tasks')
    allow_parser.add_argument('user_login', help='user who will get access to task')
    allow_parser.add_argument('-ar', '--read', action='store_true', help='allow user read task')
    allow_parser.add_argument('-aw', '--write', action='store_true', help='allow user write task')
    allow_parser.add_argument('-ad', '--disallow', action='store_true', help='disallow user access task')
    allow_parser.set_defaults(which='allow')

    return {'task': task_parser, 'remind': reminder_parser, 'allow': allow_parser}


def task_parse_args(parser, args, manager, user):
    if args.which != 'task':
        return

    if args.info:
        task = manager.get_task(user_login=user,
                                task_id=args.task_id)
        subtasks = manager.get_task_subtasks(task_id=task.id)
        print(task_full_format(task=task, subtasks=subtasks))

    if args.new_task_name:
        manager.edit_task_name(user_login=user,
                               task_id=args.task_id,
                               new_name=args.new_task_name)

    if args.new_task_desc:
        manager.edit_task_description(user_login=user,
                                      task_id=args.task_id,
                                      new_desc=args.new_task_desc)

    if args.new_task_pr:
        manager.edit_task_priority(user_login=user,
                                   task_id=args.task_id,
                                   new_priority=int(args.new_task_pr))

    if args.new_task_ed:
        manager.edit_task_end_date(user_login=user,
                                   task_id=args.task_id,
                                   new_end_date=args.new_task_ed)

    if args.add_subtask_id:
        manager.add_subtask(user_login=user,
                            task_id=args.task_id,
                            subtask_id=args.add_subtask_id)

    if args.remove_subtask_id:
        manager.delete_subtask(user_login=user,
                               task_id=args.task_id,
                               subtask_id=args.remove_subtask_id)

    if not (args.info or
            args.new_task_name or
            args.new_task_desc or
            args.new_task_pr or
            args.new_task_ed or
            args.add_subtask_id or
            args.remove_subtask_id):
        parser.error('no one optional argument received')


def parse_args(parser, remind_parser, allow_parser, args, manager, user):
    task_parse_args(parser, args, manager, user)
    allow_parse_args(allow_parser, args, manager, user)
    delta = reminder_parse_args(remind_parser, args)
    if delta is False:
        manager.edit_task_reminder_time(user_login=user,
                                        task_id=args.task_id,
                                        new_reminder_time=None)
    if delta is not None:
        manager.edit_task_reminder_time(user_login=user,
                                        task_id=args.task_id,
                                        new_reminder_time=delta)


def reminder_parse_args(parser, args):
    if args.which != 'task_reminder':
        return
    if args.remove:
        return False
    if args.minutes or args.days or args.hours:
        minutes = args.minutes[0] if args.minutes else 0
        hours = args.hours[0] if args.hours else 0
        days = args.days[0] if args.days else 0
        return datetime.timedelta(minutes=minutes, hours=hours, days=days)
    else:
        parser.error('no one optional arguments received')


def allow_parse_args(parser, args, manager, user):
    if args.which != 'allow':
        return

    if args.disallow:
        manager.task_disallow(user_login=user,
                              task_id=args.task_id,
                              disallow_user_login=args.user_login)

    elif args.write:
        manager.task_allow_write(user_login=user,
                                 task_id=args.task_id,
                                 allow_user_login=args.user_login)

    elif args.read:
        manager.task_allow_read(user_login=user,
                                task_id=args.task_id,
                                allow_user_login=args.user_login)

    else:
        parser.error('no one optional argument received')
