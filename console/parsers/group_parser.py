"""
    Parser for editing single group

"""

from console.formatters import group_full_format


def init_parser(subparsers):
    group_parser = subparsers.add_parser('group', help='one group parser')
    group_parser.add_argument('group_name', help='name of group')
    group_parser.add_argument('-a', '--add', nargs=1, type=int, help='add task to group (task id)')
    group_parser.add_argument('-r', '--remove', nargs=1, type=int, help='remove task from group (task id)')
    group_parser.add_argument('-n', '--name', dest='new_group_name', help='change group name')
    group_parser.add_argument('-i', '--info', action='store_true', help='show full group info')
    group_parser.set_defaults(which='group')

    return group_parser


def parse_args(parser, args, manager, user):
    if args.which != 'group':
        return

    group = manager.get_group(user_login=user,
                              group_name=args.group_name)

    if args.add:
        manager.group_add_task(user_login=user,
                               group_name=group.name,
                               task_id=args.add[0])

    if args.remove:
        manager.group_delete_task(user_login=user,
                                  group_name=group.name,
                                  task_id=args.remove[0])

    if args.new_group_name:
        manager.group_edit_name(user_login=user,
                                group_name=group.name,
                                new_name=args.new_group_name)

    if args.info:
        tasks = manager.get_group_tasks(user_login=user, group_name=group.name)
        print(group_full_format(group=group, tasks=tasks))

    if not (args.add or args.remove or args.new_group_name or args.info):
        parser.error('no one optional argument received')
