"""
    Parser for working with user tasks

"""


def init_parser(subparsers):
    tasks_parser = subparsers.add_parser('tasks', help='tasks commands parser')
    tasks_parser.add_argument('-c', '--create', dest='new_task_name', help='create new empty task')
    tasks_parser.add_argument('-d', '--do', type=int, dest='do_task_id',
                              help='make task executed (task id you want to do)')
    tasks_parser.add_argument('-f', '--fail', type=int, dest='fail_task_id',
                              help='make task failed (task id you want to fail)')
    tasks_parser.add_argument('-a', '--archive', type=int, dest='archive_task_id',
                              help='send task to archive (task id you want to send)')
    tasks_parser.add_argument('-r', '--remove', type=int, dest='remove_task_id',
                              help='remove task (task id you want to remove)')
    tasks_parser.set_defaults(which='tasks')

    return tasks_parser


def parse_args(parser, args, manager, user):
    if args.which != 'tasks':
        return

    if args.do_task_id:
        manager.perform_task(user_login=user,
                             task_id=args.do_task_id)

    if args.fail_task_id:
        manager.fail_task(user_login=user,
                          task_id=args.fail_task_id)

    if args.archive_task_id:
        manager.send_to_archive(user_login=user,
                                task_id=args.archive_task_id)

    if args.remove_task_id:
        manager.delete_task(user_login=user,
                            task_id=args.remove_task_id)

    if args.new_task_name:
        manager.create_task(user_login=user,
                            name=args.new_task_name)

    if not (args.do_task_id or
            args.fail_task_id or
            args.archive_task_id or
            args.remove_task_id or
            args.new_task_name):
        parser.error('no one optional argument received')
