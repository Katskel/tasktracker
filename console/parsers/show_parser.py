"""
    Parser for showing user tasks
"""

from lib.models.task import TaskStatus
from console.formatters import task_short_format


def init_parser(subparsers):
    show_parser = subparsers.add_parser('show', help='helps to show tasks')
    show_parser.add_argument('-a', '--all', action='store_true', help='show all user tasks')
    show_parser.add_argument('-ac', '--active', action='store_true', help='show all active user tasks')
    show_parser.add_argument('-ar', '--archive', action='store_true', help='show all archived user tasks')
    show_parser.add_argument('-d', '--done', action='store_true', help='show all done user tasks')
    show_parser.add_argument('-f', '--failed', action='store_true', help='show all failed user tasks')
    show_parser.add_argument('-ns', '--not_started', action='store_true', help='show all not started user tasks')
    show_parser.set_defaults(which='show')

    return show_parser


def parse_args(parser, args, manager, user):
    if args.which != 'show':
        return

    task_filter = None
    if args.all:
        task_filter = lambda task: True
    if args.active:
        task_filter = lambda task: task.status == TaskStatus.IN_PROCESS
    if args.archive:
        task_filter = lambda task: task.in_archive is True
    if args.done:
        task_filter = lambda task: task.status == TaskStatus.DONE
    if args.failed:
        task_filter = lambda task: task.status == TaskStatus.FAILED
    if args.not_started:
        task_filter = lambda task: task.status == TaskStatus.NOT_STARTED

    if task_filter is None:
        parser.error('no one optional argument received')

    tasks = manager.get_tasks(user_login=user, filter=task_filter)
    if len(tasks) == 0:
        print('No tasks')
        return

    for task in tasks:
        print(task_short_format(task=task))
