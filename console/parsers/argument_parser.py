import argparse
import sys


class ArgumentParser(argparse.ArgumentParser):
    """
        Class that overrides error method to print
        help messages
    """

    def error(self, message):
        print(message)
        self.print_help()
        sys.exit(2)
