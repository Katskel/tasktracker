"""
    Package with parsers logic and output

    Modules:
        parsers - module contains init and paser functions for all parsers
        group_parser - parser for editing single group
        groups_parser - parser for work with groups
        task_parser - parser for editing single task
        tasks_parser - parser for working with user tasks
        show_parser - parser for showing user tasks
        plan_parser - parser for editing single plan
        plans_parser - parser for working with user plans
        argument_parser - help class derived from argparse.ArgumentParser

"""