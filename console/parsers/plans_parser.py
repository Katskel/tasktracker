"""
    Parser for working with user plans
"""

import re
import datetime
from console.formatters import plan_short_format


def init_parser(subparsers):
    plans_parser = subparsers.add_parser("plans", help='work with plans')

    plans_parser.add_argument('-s', '--show', action='store_true', help='show user plans')
    plans_parser.add_argument('-r', '--remove', nargs=1, type=int, help='remove plan')
    plans_parser.set_defaults(which='plans')

    add_subparser = plans_parser.add_subparsers(help='allow to add plans')
    add_parser = add_subparser.add_parser("add")
    add_parser.set_defaults(which='add_plan')
    add_parser.add_argument('plan_name', help='name for created tasks')
    add_parser.add_argument('end_time',
                            help='end date for the first plan task, format "DD-MM-YYYY HH:MM"',
                            type=lambda d: datetime.datetime.strptime(d, '%d.%m.%Y %H:%M'))
    add_parser.add_argument('-r', '--repeat', dest='plan_repeat', help='planner will add task r times', type=int)
    add_parser.add_argument('-hr', '--hours', dest='plan_hours', help='planner will add task every h hours', type=int)
    add_parser.add_argument('-m', '--minutes', dest='plan_minutes', help='planner will add task every m minutes',
                            type=int)
    add_parser.add_argument('-d', '--days', dest='plan_days', help='planner will add task every d days', type=int)
    add_parser.add_argument('-w', '--week', dest='plan_week_days', help='planner will add task on this week days',
                            type=lambda t: re.search('^1?2?3?4?5?6?7?$', t).group(0))

    return {'plan': plans_parser, 'add': add_parser}


def parse_args(parser, add_parser, args, manager, user):
    plans_parse(parser, args, manager, user)
    add_plan_parse(add_parser, args, manager, user)


def plans_parse(parser, args, manager, user):
    if args.which != 'plans':
        return

    if args.show:
        plans = manager.get_plans(user_login=user)
        for plan in plans:
            print(plan_short_format(plan=plan))

    if args.remove:
        manager.delete_plan(user_login=user, plan_id=args.remove[0])

    if not (args.show or args.remove):
        parser.error('no one optional argument received')


def add_plan_parse(parser, args, manager, user):
    if args.which != 'add_plan':
        return

    plan_repeat = args.plan_repeat if args.plan_repeat else None
    plan_week = None
    time_delta = None
    if args.plan_week_days:
        plan_week = [int(item) for item in list(args.plan_week_days)]

    if args.plan_days or args.plan_hours or args.plan_minutes:
        days = args.plan_days if args.plan_days else 0
        hours = args.plan_hours if args.plan_hours else 0
        minutes = args.plan_minutes if args.plan_minutes else 0
        time_delta = datetime.timedelta(days=days, hours=hours, minutes=minutes)

    if plan_week is None and time_delta is None:
        parser.error('no one of time_delta and plan_week arguments received')

    manager.create_plan(user_login=user,
                        task_name=args.plan_name,
                        end_time=args.end_time,
                        repeat_days=plan_week,
                        repeat_count=plan_repeat,
                        repeat_time_delta=time_delta)
