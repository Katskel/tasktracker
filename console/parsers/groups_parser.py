"""
    Parser for work with groups

"""

from console.formatters import group_short_format


def init_parser(subparsers):
    groups_parser = subparsers.add_parser("groups", help='groups parser')
    groups_parser.add_argument('-a', '--add', nargs=1, dest='add_group', help='Add new group')
    groups_parser.add_argument('-r', '--remove', nargs=1, dest='remove_group', help='Remove group')
    groups_parser.add_argument('-s', '--show', action='store_true', help='Show user groups')
    groups_parser.set_defaults(which='groups')

    return groups_parser


def parse_args(parser, args, manager, user):
    if args.which != 'groups':
        return

    if args.add_group:
        manager.create_group(user_login=user, group_name=args.add_group[0])

    if args.remove_group:
        manager.delete_group(user_login=user, group_name=args.remove_group[0])

    if args.show:
        groups = manager.get_groups(user_login=user)
        if len(groups) == 0:
            print('No groups')
            return
        for group in groups:
            print(group_short_format(group=group))

    if not (args.add_group or args.remove_group or args.show):
        parser.error('no one optional argument received')
