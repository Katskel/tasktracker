"""
    Parser for editing single plan
"""

import re
import datetime


def init_parser(subparsers):
    plan_parser = subparsers.add_parser('plan', help='single plan editing')
    plan_parser.add_argument('plan_id', type=int, help='plan id')
    plan_parser.add_argument('-n', '--name', dest='new_plan_name', help='change plan task name')
    plan_parser.add_argument('-de', '--desc', dest='new_plan_desc', help='change plan task description')
    plan_parser.add_argument('-p', '--priority', dest='new_plan_pr', choices=['0', '1', '2', '3'],
                             help='change plan task priority')
    plan_parser.add_argument('-r', '--repeat', dest='plan_repeat', help='planner will add task r times', type=int)
    plan_parser.add_argument('-hr', '--hours', dest='plan_hours', help='planner will add task every h hours', type=int)
    plan_parser.add_argument('-m', '--minutes', dest='plan_minutes', help='planner will add task every m minutes', type=int)
    plan_parser.add_argument('-d', '--days', dest='plan_days', help='planner will add task every d days', type=int)
    plan_parser.add_argument('-w', '--week', dest='plan_week_days', help='planner will add task on this week days',
                             type=lambda t: re.search('^1?2?3?4?5?6?7?$', t).group(0))
    plan_parser.set_defaults(which='plan')
    remind_subparser = plan_parser.add_subparsers()
    reminder_parser = remind_subparser.add_parser('remind', help='set time for reminder')
    reminder_parser.add_argument('-m', '--minutes', nargs=1, type=int, help='remind m minutes before')
    reminder_parser.add_argument('-hr', '--hours', nargs=1, type=int, help='remind h hours before')
    reminder_parser.add_argument('-d', '--days', nargs=1, type=int, help='remind d days before')
    reminder_parser.add_argument('-rm', '--remove', action='store_true', help='delete reminder')
    reminder_parser.set_defaults(which='plans_reminder')

    return {"plan": plan_parser, "reminder": reminder_parser}


def plan_parse_args(parser, args, manager, user):
    if args.which != 'plan':
        return

    if args.new_plan_name:
        manager.edit_plan_name(user_login=user,
                               plan_id=args.plan_id,
                               new_name=args.new_plan_name)

    if args.new_plan_desc:
        manager.edit_plan_description(user_login=user,
                                      plan_id=args.plan_id,
                                      new_desc=args.new_plan_desc)

    if args.new_plan_pr:
        manager.edit_plan_priority(user_login=user,
                                   plan_id=args.plan_id,
                                   new_priority=int(args.new_plan_pr))

    if args.plan_repeat:
        manager.edit_plan_repeat(user_login=user,
                                 plan_id=args.plan_id,
                                 new_repeat=args.plan_repeat)

    if args.plan_week_days:
        manager.edit_plan_week(user_login=user,
                               plan_id=args.plan_id,
                               new_week=[int(item) for item in list(args.plan_week_days)])

    if args.plan_days or args.plan_hours or args.plan_minutes:
        days = args.plan_days if args.plan_days else 0
        hours = args.plan_hours if args.plan_hours else 0
        minutes = args.plan_minutes if args.plan_minutes else 0

        time_delta = datetime.timedelta(days=days, hours=hours, minutes=minutes)
        manager.edit_plan_time_delta(user_login=user,
                                     plan_id=args.plan_id,
                                     new_delta=time_delta)

    if not (args.new_plan_name or
            args.new_plan_desc or
            args.new_plan_pr or
            args.plan_repeat or
            args.plan_week_days or
            args.plan_days or
            args.plan_hours or
            args.plan_minutes):
        parser.error('no one optional argument required')


def parse_args(parser, remind_parser, args, manager, user):
    plan_parse_args(parser, args, manager, user)
    delta = reminder_parse_args(remind_parser, args)
    if delta is False:
        manager.edit_plan_reminder_time(user_login=user,
                                        plan_id=args.plan_id,
                                        new_reminder_time=None)
    if delta is not None:
        manager.edit_plan_reminder_time(user_login=user,
                                        plan_id=args.plan_id,
                                        new_reminder_time=delta)


def reminder_parse_args(parser, args):
    if args.which != 'plans_reminder':
        return
    if args.remove:
        return False
    if args.minutes or args.days or args.hours:
        minutes = args.minutes[0] if args.minutes else 0
        hours = args.hours[0] if args.hours else 0
        days = args.days[0] if args.days else 0
        return datetime.timedelta(minutes=minutes, hours=hours, days=days)
    else:
        parser.error('no one optional argument required')
