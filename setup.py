from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='TaskTracker',
    version='1.0',
    author='Pavel Katskel',
    author_email='p.katskel@gmail.com',
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    test_suite='tests.tests.get_tests',

    entry_points={
        'console_scripts':
            [
                'tasktracker=console.tasktracker:main',
                'taskconfig=console.config_worker:main'
            ]
        },

    include_package_data=True
)
