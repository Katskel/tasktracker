#TaskTracker

## About application
TaskTracker is an application created for monitoring your personal time. You can easy create tasks, set up different task options, like dates, priorities or notifications. Division of tasks into groups will allow you to quickly find out all your plans at work, family, recreation of something else. Always forget to do usually things? Create a plan that will create tasks for you. Application has also opportunity to allow other users read or edit your tasks.

##Install:
* At first, clone repository with project:
```bash 
git clone https://Katskel@bitbucket.org/Katskel/tasktracker.git
``` 
* To install application, type two commands:
```bash
pip3 install -r requirements.txt
[sudo] python3 setup.py install    
```
* Now, you have tasktracker installed. Before work you need to configure app.
Type next command:
```bash
taskconfig -d
(or) 
taskconfig --default
```
This command create standard config file with simple settings. To have more information how to set custom settings type:
```bash
taskconfig -h
```

Now you need to choose user. You can do that typing next command:
```bash
taskconfig -u user_name
or
taskconfig --user user_name
```
Example:
```bash
taskconfig -u Katskel 
```

Application is ready to work. 


##Work with console app:
Basic usage:
```bash
tasktracker [commands] [args]
```
As example:
```bash
#Create task walk a dog with deadline on 25th June 2018, 6 p.m., remind for 2 hours
tasktracker tasks --create 'walk the dog'
tasktracker task 1 --edate '25.06.2018 18:00'
tasktracker task 1 remind --hours 2

#Create user group names family
tasktracker groups --add 'family'

#Add task 'walk the dog' to family group
tasktracker group family --add 1 

```
#### Commands:
* tasks - allows you to work with tasks: add new tasks or change statuses of tasks(fail, done and other)
* task - allows you to works with single task (edit fields, add subtasks)
* show - allows you show tasks you want: active, failed, all tasks, etc.
* groups - allows you to works with groups (add/delete/show groups)
* group - allows you to work with single user group (change name or add/delete tasks)
* plans - allows you create add/remove/show plans - entities, that can create task by some special rules
* plan - allows you edit plan settings

To see all commands or help to some subcommands type -h:  

Good luck and use your time properly!