import unittest
from lib.logger import disable_logger
from lib.task_tracker import TaskTracker
from lib.storage.json_storage import JsonStorage
from lib.exceptions import PermissionDenied, GroupNotFound
import os


def start_test():
    unittest.main()


class GroupTests(unittest.TestCase):
    def setUp(self):
        disable_logger()
        path = os.path.join(os.path.abspath(os.curdir), 'test_dir')
        storage = JsonStorage(path)
        self.manager = TaskTracker(storage)

    def test_create_group(self):
        group_id = self.manager.create_group(user_login='test', group_name='group1')
        group = self.manager.get_group(user_login='test', group_name='group1')
        self.assertTrue(group_id == group.id)

        try:
            group_id = self.manager.create_group(user_login='test', group_name='group1')
        except ValueError:
            pass
        except Exception:
            raise

    def test_add_delete_tasks(self):
        self.manager.create_group(user_login='test', group_name='group2')
        task_id = self.manager.create_task(user_login='test', name='test task')

        self.manager.create_group(user_login='fake', group_name='group2')
        try:
            self.manager.group_add_task(user_login='fake', group_name='group2', task_id=task_id)
        except PermissionDenied:
            pass
        except Exception:
            raise

        self.manager.group_add_task(user_login='test', group_name='group2', task_id=task_id)

        group_tasks = self.manager.get_group_tasks(user_login='test', group_name='group2')
        self.assertTrue(len(group_tasks) == 1)

        try:
            self.manager.group_add_task(user_login='test', group_name='group2', task_id=task_id)
        except ValueError:
            pass
        except Exception:
            raise

        self.manager.group_delete_task(user_login='test', group_name='group2', task_id=task_id)

        group_tasks = self.manager.get_group_tasks(user_login='test', group_name='group2')
        self.assertTrue(len(group_tasks) == 0)

    def test_group_edit(self):
        group3_id = self.manager.create_group(user_login='test', group_name='group3')
        self.manager.group_edit_name(user_login='test', group_name='group3', new_name='new group name')
        try:
            group3_new_id = self.manager.get_group(user_login='test', group_name='group3')
        except GroupNotFound:
            pass
        except Exception:
            raise
        group3_new = self.manager.get_group(user_login='test', group_name='new group name')
        self.assertTrue(group3_id == group3_new.id)

        try:
            self.manager.group_edit_name(user_login='fake', group_name='new group name', new_name='new name 2')
        except GroupNotFound:
            pass
        except Exception:
            raise

    def test_get_groups(self):
        self.manager.create_group(user_login='test2', group_name='group1')
        self.manager.create_group(user_login='test2', group_name='group2')
        self.manager.create_group(user_login='test2', group_name='group3')

        groups = self.manager.get_groups(user_login='test2')

        self.assertTrue(len(groups) == 3)

    def tearDown(self):
        self.manager.storage.clear_storage()


if __name__ == "__main__":
    start_test()
