import unittest
from tests.group_test import GroupTests
from tests.task_test import TaskTests
from tests.plans_test import PlanTests


def get_tests():
    suite = unittest.TestSuite()

    suite.addTests(unittest.makeSuite(GroupTests))
    suite.addTests(unittest.makeSuite(TaskTests))
    suite.addTests(unittest.makeSuite(PlanTests))

    return suite
