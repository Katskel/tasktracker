import unittest
from lib.logger import disable_logger
from lib.task_tracker import TaskTracker
from lib.models.task import TaskPriority, TaskStatus
from lib.models.task_action import ActionType
from lib.exceptions import PermissionDenied, TaskNotFound
from lib.storage.json_storage import JsonStorage
import datetime
import os


def start_test():
    unittest.main()


class TaskTests(unittest.TestCase):
    def setUp(self):
        disable_logger()
        path = os.path.join(os.path.abspath(os.curdir), 'test_dir')
        storage = JsonStorage(path)
        self.manager = TaskTracker(storage)

    def test_task_create(self):
        task_id = self.manager.create_task(user_login='test', name='test name')
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.name == 'test name')

    def test_send_to_archive(self):
        task_id = self.manager.create_task(user_login='test', name='task2')
        self.manager.send_to_archive(user_login='test', task_id=task_id)
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.is_archive is True)

        task2_id = self.manager.create_task(user_login='fake', name='test3')
        try:
            self.manager.send_to_archive(user_login='test', task_id=task2_id)
        except PermissionDenied:
            pass
        except Exception:
            raise

    def test_perform_task(self):
        task_id = self.manager.create_task(user_login='test', name='task4')
        self.manager.perform_task(user_login='test', task_id=task_id)
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.status == TaskStatus.DONE)

        task2_id = self.manager.create_task(user_login='fake', name='test5')
        try:
            self.manager.perform_task(user_login='test', task_id=task2_id)
        except PermissionDenied:
            pass
        except Exception:
            raise

    def test_start_task(self):
        task_id = self.manager.create_task(user_login='test', name='task6')
        self.manager.start_task(user_login='test', task_id=task_id)
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.status == TaskStatus.IN_PROCESS)

        task2_id = self.manager.create_task(user_login='fake', name='test7')
        try:
            self.manager.start_task(user_login='test', task_id=task2_id)
        except PermissionDenied:
            pass
        except Exception:
            raise

    def test_stop_task(self):
        task_id = self.manager.create_task(user_login='test', name='task8')
        self.manager.stop_task(user_login='test', task_id=task_id)
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.status == TaskStatus.NOT_STARTED)

        task2_id = self.manager.create_task(user_login='fake', name='test9')
        try:
            self.manager.perform_task(user_login='test', task_id=task2_id)
        except PermissionDenied:
            pass
        except Exception:
            raise

    def test_fail_task(self):
        task_id = self.manager.create_task(user_login='test', name='task10')
        self.manager.fail_task(user_login='test', task_id=task_id)
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.status == TaskStatus.FAILED)

        task2_id = self.manager.create_task(user_login='fake', name='test11')
        try:
            self.manager.perform_task(user_login='test', task_id=task2_id)
        except PermissionDenied:
            pass
        except Exception:
            raise

    def test_task_edit_name(self):
        task_id = self.manager.create_task(user_login='test', name='task12')
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.name == 'task12')

        self.manager.edit_task(user_login='test',
                               task_id=task_id,
                               new_name='task12_new')
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.name == 'task12_new')

        try:
            self.manager.edit_task(user_login='fake',
                                   task_id=task_id,
                                   new_name='task12_new2')
        except PermissionDenied:
            pass
        except Exception:
            raise

        try:
            self.manager.edit_task(user_login='test',
                                   task_id=task_id,
                                   new_name='')
        except ValueError:
            pass
        except Exception:
            raise

    def test_task_edit_description(self):
        task_id = self.manager.create_task(user_login='test', name='task13')
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.description == '')

        self.manager.edit_task(user_login='test',
                               task_id=task_id,
                               new_description='description for task')
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.description == 'description for task')

        try:
            self.manager.edit_task(user_login='fake',
                                   task_id=task_id,
                                   new_description='fake description')
        except PermissionDenied:
            pass
        except Exception:
            raise

    def test_task_edit_reminder(self):
        task_id = self.manager.create_task(user_login='test', name='task14')
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.reminder_time is None)

        self.manager.edit_task(user_login='test',
                               task_id=task_id,
                               new_reminder_time=datetime.timedelta(hours=5))
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.reminder_time == datetime.timedelta(hours=5))

        try:
            self.manager.edit_task(user_login='fake',
                                   task_id=task_id,
                                   new_reminder_time=datetime.timedelta(hours=5))
        except PermissionDenied:
            pass
        except Exception:
            raise

        try:
            self.manager.edit_task(user_login='test',
                                   task_id=task_id,
                                   new_reminder_time=datetime.timedelta(hours=-5))
        except ValueError:
            pass
        except Exception:
            raise

    def test_edit_task_end_date(self):
        task_id = self.manager.create_task(user_login='test', name='task15')
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.end_date is None)

        self.manager.edit_task(user_login='test',
                               task_id=task_id,
                               new_end_date=datetime.datetime(2018, 6, 9, 17, 0))
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.end_date == datetime.datetime(2018, 6, 9, 17, 0))

        try:
            self.manager.edit_task(user_login='fake',
                                   task_id=task_id,
                                   new_end_date=datetime.datetime(2018, 6, 9, 17, 0))
        except PermissionDenied:
            pass
        except Exception:
            raise

    def test_edit_task_priority(self):
        task_id = self.manager.create_task(user_login='test', name='task16')
        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.priority == TaskPriority.NOT)

        self.manager.edit_task(user_login='test',
                               task_id=task_id,
                               new_priority=2)

        task = self.manager.get_task(user_login='test', task_id=task_id)
        self.assertTrue(task.priority == TaskPriority.MEDIUM)

        try:
            self.manager.edit_task(user_login='fake',
                                   task_id=task_id,
                                   new_priority=3)
        except PermissionDenied:
            pass
        except Exception:
            raise

    def test_task_subtasks(self):
        task5_id = self.manager.create_task(user_login='test', name='test5')
        task6_id = self.manager.create_task(user_login='test', name='test6')
        self.manager.add_subtask(user_login='test', task_id=task5_id, subtask_id=task6_id)
        task6 = self.manager.get_task(user_login='test', task_id=task6_id)
        self.assertTrue(task6.head_task_id == task5_id)
        try:
            self.manager.add_subtask(user_login='test', task_id=task6_id, subtask_id=task5_id)
        except PermissionDenied:
            pass
        except Exception:
            raise
        self.manager.delete_subtask(user_login='test', task_id=task5_id, subtask_id=task6_id)
        task6 = self.manager.get_task(user_login='test', task_id=task6_id)
        self.assertTrue(task6.head_task_id is None)

    def test_task_delete(self):
        task1_id = self.manager.create_task(user_login='test', name='task17')
        task2_id = self.manager.create_task(user_login='test', name='task18')

        self.manager.delete_task(user_login='test', task_id=task1_id)
        try:
            task = self.manager.get_task(user_login='test', task_id=task1_id)
        except TaskNotFound:
            pass
        except Exception:
            raise

        try:
            self.manager.delete_task(user_login='fake', task_id=task2_id)
        except PermissionDenied:
            pass
        except Exception:
            raise

    def test_get_task_action(self):
        task_id = self.manager.create_task(user_login='test', name='task19')

        action = self.manager.get_task_action(user_login='test', task_id=task_id)
        self.assertTrue(action == ActionType.WRITE)

        fake_action = self.manager.get_task_action(user_login='fake', task_id=task_id)

        self.assertTrue(fake_action is None)

    def test_get_task_subtasks(self):
        task20_id = self.manager.create_task(user_login='test', name='task20')
        task21_id = self.manager.create_task(user_login='test', name='subtask21')
        task22_id = self.manager.create_task(user_login='test', name='subtask22')
        self.manager.add_subtask(user_login='test', task_id=task20_id, subtask_id=task21_id)
        self.manager.add_subtask(user_login='test', task_id=task20_id, subtask_id=task22_id)

        subtasks = self.manager.get_task_subtasks(task20_id)

        self.assertTrue(len(subtasks) == 2)

    def test_task_allow(self):
        task21_id = self.manager.create_task(user_login='test', name='task23')

        try:
            task = self.manager.get_task(user_login='fake', task_id=task21_id)
        except PermissionDenied:
            pass
        except Exception:
            raise

        self.manager.task_allow_read(user_login='test', task_id=task21_id, allow_user_login='fake')

        task = self.manager.get_task(user_login='fake', task_id=task21_id)

        try:
            self.manager.edit_task(user_login='fake',
                                   task_id=task21_id,
                                   new_name='task23new')
        except PermissionDenied:
            pass
        except Exception:
            raise

        self.manager.task_allow_write(user_login='test', task_id=task21_id, allow_user_login='fake')

        self.manager.edit_task(user_login='fake',
                               task_id=task21_id,
                               new_name='task23new')

        task = self.manager.get_task(user_login='test', task_id=task21_id)
        self.assertTrue(task.name == 'task23new')

        self.manager.task_disallow(user_login='test', task_id=task21_id, disallow_user_login='fake')

        try:
            task = self.manager.get_task(user_login='fake', task_id=task21_id)
        except PermissionDenied:
            pass
        except Exception:
            raise

    def tearDown(self):
        self.manager.storage.clear_storage()


if __name__ == "__main__":
    start_test()
