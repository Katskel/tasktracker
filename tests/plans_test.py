import unittest
from _cffi_backend import new_primitive_type

from lib.logger import disable_logger
from lib.task_tracker import TaskTracker
from lib.models.task import TaskPriority
from lib.exceptions import PermissionDenied
from lib.storage.json_storage import JsonStorage
import datetime
import os


def start_test():
    unittest.main()


class PlanTests(unittest.TestCase):
    def setUp(self):
        disable_logger()
        path = os.path.join(os.path.abspath(os.curdir), 'test_dir')
        storage = JsonStorage(path)
        self.manager = TaskTracker(storage)

    def test_create_plan(self):
        plan_id = self.manager.create_plan(
            user_login='test',
            task_name='plan 1',
            end_time=datetime.datetime(year=2018, day=5, month=6, hour=15),
            repeat_days=[1, 2, 4],
            repeat_count=4,
            repeat_time_delta=datetime.timedelta(hours=2),
            reminder_time=datetime.timedelta(minutes=50),
            task_description='no desc',
            task_priority=1
            )

        plan = self.manager.storage.get_plan_by_id(plan_id)

        self.assertTrue(plan.task_name == 'plan 1')
        self.assertTrue(plan.end_date == datetime.datetime(year=2018, day=5, month=6, hour=15))
        self.assertTrue(plan.repeat_days == [1, 2, 4])
        self.assertTrue(plan.repeat_count == 4)
        self.assertTrue(plan.repeat_time_delta == datetime.timedelta(hours=2))
        self.assertTrue(plan.reminder_time == datetime.timedelta(minutes=50))
        self.assertTrue(plan.task_description == 'no desc')
        self.assertTrue(plan.task_priority == TaskPriority.LOW)

    def test_delete_plan(self):
        plan2_id = self.manager.create_plan(
            user_login='test_user',
            task_name='plan 2',
            end_time=datetime.datetime(year=2018, day=5, month=6, hour=15),
            repeat_days=[1, 2, 4],
            repeat_count=4,
            repeat_time_delta=datetime.timedelta(hours=2),
            reminder_time=datetime.timedelta(minutes=50),
            task_description='no desc',
            task_priority=1
        )

        plan3_id = self.manager.create_plan(
            user_login='test_user',
            task_name='plan 3',
            end_time=datetime.datetime(year=2018, day=5, month=6, hour=15),
            repeat_days=[2, 5, 6, 7],
            repeat_count=2,
            repeat_time_delta=datetime.timedelta(minutes=40),
            reminder_time=datetime.timedelta(minutes=50),
            task_description='',
            task_priority=2
        )

        plans = self.manager.get_plans(user_login='test_user')

        self.assertTrue(len(plans) == 2)

        try:
            self.manager.delete_plan(user_login='test', plan_id=plan2_id)
        except PermissionDenied:
            pass
        except Exception:
            raise

        self.manager.delete_plan(user_login='test_user', plan_id=plan2_id)

        plans = self.manager.get_plans(user_login='test_user')

        self.assertTrue(len(plans) == 1)

    def test_edit_plan_name(self):
        plan4_id = self.manager.create_plan(
            user_login='test_user',
            task_name='plan 4',
            end_time=datetime.datetime(year=2018, day=5, month=6, hour=15),
            repeat_days=[2, 5, 6, 7],
            repeat_count=2,
            repeat_time_delta=datetime.timedelta(minutes=40),
            reminder_time=datetime.timedelta(minutes=50),
            task_description='',
            task_priority=2
        )

        plan = self.manager.storage.get_plan_by_id(plan4_id)
        self.assertTrue(plan.task_name == 'plan 4')

        self.manager.edit_plan(user_login='test_user',
                               plan_id=plan4_id,
                               new_name='plan 4 new')

        plan = self.manager.storage.get_plan_by_id(plan4_id)
        self.assertTrue(plan.task_name == 'plan 4 new')

        try:
            self.manager.edit_plan(user_login='fake_user',
                                   plan_id=plan4_id,
                                   new_name='plan 4 fake new')
        except PermissionDenied:
            pass
        except Exception:
            raise

        try:
            self.manager.edit_plan(user_login='test_user',
                                   plan_id=plan4_id,
                                   new_name='')
        except ValueError:
            pass
        except Exception:
            raise

    def test_edit_plan_priority(self):
        plan5_id = self.manager.create_plan(
            user_login='test_user',
            task_name='plan 5',
            end_time=datetime.datetime(year=2018, day=5, month=6, hour=15),
            repeat_days=[2, 5, 6, 7],
            repeat_count=2,
            repeat_time_delta=datetime.timedelta(minutes=40),
            reminder_time=datetime.timedelta(minutes=50),
            task_description='',
            task_priority=2
        )

        plan = self.manager.storage.get_plan_by_id(plan5_id)


        self.assertTrue(plan.task_priority == TaskPriority.MEDIUM)

        self.manager.edit_plan(user_login='test_user',
                               plan_id=plan5_id,
                               new_priority=0)

        plan = self.manager.storage.get_plan_by_id(plan5_id)
        self.assertTrue(plan.task_priority == TaskPriority.NOT)

        try:
            self.manager.edit_plan(user_login='fake_user',
                                   plan_id=plan5_id,
                                   new_priority=1)
        except PermissionDenied:
            pass
        except Exception:
            raise

    def test_edit_plan_description(self):
        plan6_id = self.manager.create_plan(
            user_login='test_user',
            task_name='plan 6',
            end_time=datetime.datetime(year=2018, day=5, month=6, hour=15),
            repeat_days=[2, 5, 6, 7],
            repeat_count=2,
            repeat_time_delta=datetime.timedelta(minutes=40),
            reminder_time=datetime.timedelta(minutes=50),
            task_description='',
            task_priority=2
        )

        plan = self.manager.storage.get_plan_by_id(plan6_id)
        self.assertTrue(plan.task_description == '')

        self.manager.edit_plan(user_login='test_user',
                               plan_id=plan6_id,
                               new_description='new plan description')

        plan = self.manager.storage.get_plan_by_id(plan6_id)
        self.assertTrue(plan.task_description == 'new plan description')

        try:
            self.manager.edit_plan(user_login='fake_user',
                                   plan_id=plan6_id,
                                   new_description='new plan fake description')
        except PermissionDenied:
            pass
        except Exception:
            raise

    def test_plan_edit_reminder(self):
        plan7_id = self.manager.create_plan(
            user_login='test_user',
            task_name='plan 7',
            end_time=datetime.datetime(year=2018, day=5, month=6, hour=15),
            repeat_days=[2, 5, 6, 7],
            repeat_count=2,
            repeat_time_delta=datetime.timedelta(minutes=40),
            reminder_time=datetime.timedelta(minutes=50),
            task_description='',
            task_priority=2
        )

        plan = self.manager.storage.get_plan_by_id(plan7_id)

        self.assertTrue(plan.reminder_time == datetime.timedelta(minutes=50))

        self.manager.edit_plan(user_login='test_user',
                               plan_id=plan7_id,
                               new_reminder_time=datetime.timedelta(hours=5))

        plan = self.manager.storage.get_plan_by_id(plan7_id)
        self.assertTrue(plan.reminder_time == datetime.timedelta(hours=5))

        try:
            self.manager.edit_plan(user_login='fake',
                                   plan_id=plan7_id,
                                   new_reminder_time=datetime.timedelta(hours=5))
        except PermissionDenied:
            pass
        except Exception:
            raise

        try:
            self.manager.edit_plan(user_login='test_user',
                                   plan_id=plan7_id,
                                   new_reminder_time=datetime.timedelta(hours=-5))
        except ValueError:
            pass
        except Exception:
            raise

    def test_plan_edit_time_delta(self):
        plan8_id = self.manager.create_plan(
            user_login='test_user',
            task_name='plan 8',
            end_time=datetime.datetime(year=2018, day=5, month=6, hour=15),
            repeat_days=[2, 5, 6, 7],
            repeat_count=2,
            repeat_time_delta=datetime.timedelta(minutes=40),
            reminder_time=datetime.timedelta(minutes=50),
            task_description='',
            task_priority=2
        )

        plan = self.manager.storage.get_plan_by_id(plan8_id)

        self.assertTrue(plan.repeat_time_delta == datetime.timedelta(minutes=40))

        self.manager.edit_plan(user_login='test_user',
                               plan_id=plan8_id,
                               new_delta=datetime.timedelta(hours=2))

        plan = self.manager.storage.get_plan_by_id(plan8_id)
        self.assertTrue(plan.repeat_time_delta == datetime.timedelta(hours=2))

        try:
            self.manager.edit_plan(user_login='fake_user',
                                   plan_id=plan8_id,
                                   new_delta=datetime.timedelta(hours=2))
        except PermissionDenied:
            pass
        except Exception:
            raise

        try:
            self.manager.edit_plan(user_login='test_user',
                                   plan_id=plan8_id,
                                   new_delta=datetime.timedelta(hours=-22))
        except ValueError:
            pass
        except Exception:
            raise

    def test_plan_edit_repeat(self):
        plan9_id = self.manager.create_plan(
            user_login='test_user',
            task_name='plan 9',
            end_time=datetime.datetime(year=2018, day=5, month=6, hour=15),
            repeat_days=[2, 5, 6, 7],
            repeat_count=2,
            repeat_time_delta=datetime.timedelta(minutes=40),
            reminder_time=datetime.timedelta(minutes=50),
            task_description='',
            task_priority=2
        )

        plan = self.manager.storage.get_plan_by_id(plan9_id)

        self.assertTrue(plan.repeat_count == 2)

        self.manager.edit_plan(user_login='test_user',
                               plan_id=plan9_id,
                               new_repeat=4)

        plan = self.manager.storage.get_plan_by_id(plan9_id)
        self.assertTrue(plan.repeat_count == 4)

        try:
            self.manager.edit_plan(user_login='fake_user',
                                   plan_id=plan9_id,
                                   new_repeat=5)
        except PermissionDenied:
            pass
        except Exception:
            raise

        try:
            self.manager.edit_plan(user_login='test_user',
                                   plan_id=plan9_id,
                                   new_repeat=-1)
        except ValueError:
            pass
        except Exception:
            raise

    def test_plan_edit_week_days(self):
        plan10_id = self.manager.create_plan(
            user_login='test_user',
            task_name='plan 10',
            end_time=datetime.datetime(year=2018, day=5, month=6, hour=15),
            repeat_days=[2, 5, 6, 7],
            repeat_count=2,
            repeat_time_delta=datetime.timedelta(minutes=40),
            reminder_time=datetime.timedelta(minutes=50),
            task_description='',
            task_priority=2
        )

        plan = self.manager.storage.get_plan_by_id(plan10_id)

        self.assertTrue(plan.repeat_days == [2, 5, 6, 7])

        self.manager.edit_plan(user_login='test_user',
                               plan_id=plan10_id,
                               new_week=[1, 2, 4])

        plan = self.manager.storage.get_plan_by_id(plan10_id)
        self.assertTrue(plan.repeat_days == [1, 2, 4])

        try:
            self.manager.edit_plan(user_login='fake_user',
                                   plan_id=plan10_id,
                                   new_week=[1, 2, 4])
        except PermissionDenied:
            pass
        except Exception:
            raise

        try:
            self.manager.edit_plan(user_login='test_user',
                                   plan_id=plan10_id,
                                   new_week=[])
        except ValueError:
            pass
        except Exception:
            raise

    def tearDown(self):
        self.manager.storage.clear_storage()


if __name__ == "__main__":
    start_test()
