from django.conf.urls import url
from tasktracker import forms, views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^register/$', forms.RegisterFormView.as_view(), name='register'),
    url(r'^login/$', forms.LoginFormView.as_view(), name='login'),
    url(r'^accounts/login/$', forms.LoginFormView.as_view()),
    url(r'^logout/$', forms.LogoutView.as_view(), name='logout'),
    url(r'^tasks/$', views.tasks, name='tasks'),
    url(r'^tasks/edit/(?P<task_id>[0-9]+)/$', views.task_edit, name='task-edit'),
    url(r'^tasks/remove/(?P<task_id>[0-9]+)/$', views.task_remove, name='task-remove'),
    url(r'^tasks/finish/(?P<task_id>[0-9]+)/$', views.task_finish, name='task-finish'),
    url(r'^tasks/archive/(?P<task_id>[0-9]+)/$', views.task_archive, name='task-archive'),
    url(r'^tasks/add_subtask/(?P<task_id>[0-9]+)/$', views.task_add_subtask, name='task-add-subtask'),
    url(r'^tasks/remove_subtask/(?P<task_id>[0-9]+)/(?P<subtask_id>[0-9]+)/$', views.task_remove_subtask, name='task-remove-subtask'),
    url(r'^tasks/add/$', views.add_task, name='add-task'),
    url(r'^filter_tasks/$', views.filter_tasks, name='filter-tasks'),
    url(r'^groups/$', views.groups, name='groups'),
    url(r'^groups/add/$', views.group_add, name='add-group'),
    url(r'^groups/edit/(?P<group_id>[0-9]+)/$', views.group_edit, name='group-edit'),
    url(r'^groups/remove/(?P<group_id>[0-9]+)/$', views.group_remove, name='group-remove'),
    url(r'^groups/add_task/(?P<group_id>[0-9]+)/$', views.group_add_task, name='group-add-task'),
    url(r'^groups/remove_task/(?P<group_id>[0-9]+)/(?P<task_id>[0-9]+)/$', views.group_remove_task, name='group-remove-task'),
    url(r'^plans/$', views.plans, name='plans'),
    url(r'^plans/add/$', views.plan_add, name='add-plan'),
    url(r'^plans/edit/(?P<plan_id>[0-9]+)/$', views.plan_edit, name='plan-edit'),
    url(r'^plans/remove/(?P<plan_id>[0-9]+)/$', views.plan_remove, name='plan-remove'),
    url(r'^tasks/add_permission/(?P<task_id>[0-9]+)/$', views.task_add_permission, name='add-permission'),
    url(r'^tasks/remove_permission/(?P<task_id>[0-9]+)/(?P<disallow_user>[a-zA-Z0-9_.-]+)/$', views.task_remove_permission,
        name='remove-permission'),
]