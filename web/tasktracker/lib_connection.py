from tasktracker.sqlite_storage import SQLiteStorage
from django.http import HttpResponse, Http404
from django.template import loader
from lib.task_tracker import TaskTracker
from lib import exceptions
from lib.models.task import TaskStatus, TaskPriority
from tasktracker import models


def get_manager(user_login):
    storage = SQLiteStorage()
    manager = TaskTracker(storage)
    manager.update_tasks(user_login=user_login)
    manager.update_plans(user_login=user_login)
    return manager


def exceptions_wrapper(func):
    def execute(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except (exceptions.PermissionDenied,
                exceptions.GroupNotFound,
                exceptions.PlanNotFound,
                exceptions.TaskActionNotFound,
                exceptions.TaskGroupNotFound,
                exceptions.TaskNotFound,
                exceptions.UserNotFound,
                ValueError) as e:
            template = loader.get_template('error.html')
            return HttpResponse(template.render({'exception': e}))
        except Exception:
            raise

    return execute


def get_task_actions(task_id):
    task = models.Task.objects.get(id=task_id)
    actions = models.TaskAction.objects.filter(task=task)
    user_actions = []
    for action in actions:
        user = action.user.user_login
        user_action = action.action_type
        user_actions.append({'user_login': user,
                             'action_type': user_action})

    return user_actions


def get_task_statuses():
    return [(len(TaskStatus), 'All')] + [(status.value, str(status)) for status in TaskStatus]


def get_task_priorities():
    return [(len(TaskPriority), 'All')] + [(priority.value, str(priority)) for priority in TaskPriority]

