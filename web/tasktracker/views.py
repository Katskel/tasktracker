from datetime import datetime
import copy
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.template import loader
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import redirect
from tasktracker.lib_connection import (get_manager,
                             exceptions_wrapper,
                             get_task_actions)
from tasktracker.sqlite_storage import (convert_task_to_model,
                             convert_group_to_model,
                             convert_plan_to_model)
from tasktracker.models import Task, Group, Plan
from lib import exceptions
from lib.models.task_action import ActionType
from lib.models.task import (TaskPriority,
                              TaskStatus)
from tasktracker import forms


def index(request):
    template = loader.get_template('index.html')
    context = {"user_login": request.user.username}
    return HttpResponse(template.render(context))


@exceptions_wrapper
@login_required
def tasks(request, tasks=None):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    template = loader.get_template('tasks.html')
    if tasks is None:
        tasks = manager.get_tasks(user_login=user_login, is_archive=False)
    context = {
        'tasks': [convert_task_to_model(task) for task in tasks],
        "user_login": user_login
    }
    return HttpResponse(template.render(context))


@exceptions_wrapper
@login_required
def task_edit(request, task_id):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    task = manager.get_task(user_login=user_login, task_id=task_id)
    task = convert_task_to_model(task)
    old_name = copy.copy(task.name)
    old_end_date = copy.copy(datetime.strftime(task.end_date, '%Y-%m-%d %H:%M') if task.end_date is not None else None)
    old_priority = copy.copy(task.priority)
    old_reminder_time = copy.copy(task.reminder_time)
    old_description = copy.copy(task.description)
    users_actions = get_task_actions(task_id)
    if request.method == "POST":
        form = forms.TaskEditing(request.POST, instance=task)
        if form.is_valid():
            task = form.save(commit=False)
            new_name = task.name if task.name != old_name else False
            end_date = datetime.strftime(task.end_date, '%Y-%m-%d %H:%M') if task.end_date is not None else None
            end_date = end_date if end_date != old_end_date else False
            priority = task.priority if task.priority != old_priority else False
            description = task.description if task.description != old_description else False
            reminder_time = task.reminder_time if task.reminder_time != old_reminder_time else False
            manager.edit_task(user_login=user_login,
                              task_id=task.id,
                              new_end_date=end_date,
                              new_name=new_name,
                              new_priority=priority,
                              new_description=description,
                              new_reminder_time=reminder_time)
            return HttpResponseRedirect(reverse('task-edit', args=(task_id,)))
    else:
        form = forms.TaskEditing(instance=task)

    return render(request, 'task_edit.html', {   'task_id': task_id,
                                                 'subtasks': [convert_task_to_model(subtask) for subtask in manager.get_task_subtasks(task_id=task_id)],
                                                 'tasks': [convert_task_to_model(task) for task in manager.get_tasks(user_login=user_login)],
                                                 'form': form,
                                                 'action_types': [action for action in ActionType],
                                                 'users_actions': users_actions,
                                                 "user_login": user_login
                                             })


@exceptions_wrapper
@login_required
def add_task(request):
    if request.method == "POST":
        form = forms.TaskEditing(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            user_login = request.user.username
            manager = get_manager(user_login=user_login)
            manager.create_task(user_login=user_login,
                                name=task.name,
                                description=task.description,
                                reminder_time=task.reminder_time,
                                end_date=datetime.strftime(task.end_date, '%Y-%m-%d %H:%M') if task.end_date is not None else None,
                                priority=task.priority)
            return HttpResponseRedirect(reverse('tasks'))
    else:
        form = forms.TaskEditing()
    return render(request, 'task_add.html', {'form': form,
                                             "user_login": request.user.username
                                             })


@exceptions_wrapper
@login_required
def task_remove(request, task_id):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    manager.delete_task(user_login, task_id)
    return HttpResponseRedirect(reverse('tasks'))


@exceptions_wrapper
@login_required
def task_finish(request, task_id):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    manager.perform_task(user_login, task_id)
    return HttpResponseRedirect(reverse('tasks'))


@exceptions_wrapper
@login_required
def task_archive(request, task_id):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    manager.send_to_archive(user_login, task_id)
    return HttpResponseRedirect(reverse('tasks'))


@exceptions_wrapper
@login_required
def task_add_subtask(request, task_id):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    if request.method == "POST":
        subtask_id = request.POST.get('task')
        if subtask_id:
            manager.add_subtask(user_login=user_login, task_id=int(task_id), subtask_id=int(subtask_id))
    return HttpResponseRedirect(reverse('task-edit', args=(task_id,)))


@exceptions_wrapper
@login_required
def task_remove_subtask(request, task_id, subtask_id):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    manager.delete_subtask(user_login=user_login, task_id=int(task_id), subtask_id=int(subtask_id))
    return HttpResponseRedirect(reverse('task-edit', args=(task_id,)))


@exceptions_wrapper
@login_required
def groups(request):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    groups = manager.get_groups(user_login=user_login)
    return render(request, 'groups.html', {'groups': [convert_group_to_model(group) for group in groups],
                                           "user_login": user_login})


@exceptions_wrapper
@login_required
def group_remove(request, group_id):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    group = manager.storage.get_group_by_id(group_id=group_id, user_login=user_login)
    group_name = group.name
    manager.delete_group(user_login=user_login, group_name=group_name)

    return HttpResponseRedirect(reverse('groups'))


@exceptions_wrapper
@login_required
def group_add(request):
    if request.method == "POST":
        user_login = request.user.username
        manager = get_manager(user_login=user_login)
        group_name = request.POST.get('group_name')
        if group_name:
            manager.create_group(user_login=user_login, group_name=group_name)

    return HttpResponseRedirect(reverse('groups'))


@exceptions_wrapper
@login_required
def group_edit(request, group_id):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    group = manager.storage.get_group_by_id(group_id=group_id, user_login=user_login)
    group = convert_group_to_model(group)
    old_name = copy.copy(group.name)
    if request.method == "POST":
        form = forms.GroupEditing(request.POST, instance=group)
        if form.is_valid():
            group = form.save(commit=False)
            new_name = group.name if group.name != old_name else False
            manager.group_edit_name(user_login=user_login, group_name=old_name, new_name=new_name)
            return HttpResponseRedirect(reverse('group-edit', args=(group_id,)))
    else:
        form = forms.GroupEditing(instance=group)
    group = manager.storage.get_group_by_id(group_id=group_id, user_login=user_login)
    return render(request, 'group_edit.html', { 'group_id': group_id,
                                                'group_tasks': [convert_task_to_model(task) for task in manager.get_group_tasks(user_login=user_login, group_name=group.name)],
                                                'user_tasks': [convert_task_to_model(task) for task in manager.get_tasks(user_login=user_login)],
                                                'form': form,
                                                "user_login": user_login})


@exceptions_wrapper
@login_required
def group_remove_task(request, group_id, task_id):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    group = manager.storage.get_group_by_id(group_id=group_id, user_login=user_login)
    manager.group_delete_task(user_login=user_login, group_name=group.name, task_id=int(task_id))
    return HttpResponseRedirect(reverse('group-edit', args=(group_id,)))


@exceptions_wrapper
@login_required
def group_add_task(request, group_id):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    group = manager.storage.get_group_by_id(group_id=group_id, user_login=user_login)
    if request.method == "POST":
        task_id = request.POST.get('task')
        if task_id:
            manager.group_add_task(user_login=user_login, group_name=group.name, task_id=int(task_id))
    return HttpResponseRedirect(reverse('group-edit', args=(group_id,)))


@exceptions_wrapper
@login_required
def plans(request):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    template = loader.get_template('plans.html')
    context = {
        'plans': [convert_plan_to_model(plan) for plan in manager.get_plans(user_login=user_login)],
        "user_login": user_login
    }
    return HttpResponse(template.render(context))


@exceptions_wrapper
@login_required
def plan_add(request):
    if request.method == "POST":
        form = forms.PlanAdding(request.POST)
        if form.is_valid():
            plan = form.save(commit=False)
            user_login = request.user.username
            manager = get_manager(user_login=user_login)
            manager.create_plan(user_login=user_login,
                                task_name=plan.task_name,
                                end_time=datetime.strftime(plan.end_date, '%Y-%m-%d %H:%M') if plan.end_date is not None else None,
                                repeat_days=plan.repeat_days,
                                repeat_count=plan.repeat_count,
                                repeat_time_delta=plan.repeat_time_delta,
                                reminder_time=plan.reminder_time,
                                task_description=plan.task_description,
                                task_priority=plan.task_priority)
            return HttpResponseRedirect(reverse('plans'))
    else:
        form = forms.PlanAdding()
    return render(request, 'plan_edit.html', {'form': form,
                                              "user_login": request.user.username})


@exceptions_wrapper
@login_required
def plan_edit(request, plan_id):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    plan = manager.storage.get_plan_by_id(plan_id=plan_id)
    if plan.user_login != user_login:
        raise exceptions.PermissionDenied('User can not edit plan')
    plan = convert_plan_to_model(plan)
    old_name = copy.copy(plan.task_name)
    old_description = copy.copy(plan.task_description)
    old_reminder_time = copy.copy(plan.reminder_time)
    old_priority = copy.copy(plan.task_priority)
    old_week = copy.copy(plan.repeat_days)
    old_delta = copy.copy(plan.repeat_time_delta)
    old_count = copy.copy(plan.repeat_count)
    if request.method == "POST":
        form = forms.PlanEditing(request.POST, instance=plan)
        if form.is_valid():
            plan = form.save(commit=False)
            new_name = plan.task_name if plan.task_name != old_name else False
            new_desc = plan.task_description if plan.task_description != old_description else False
            new_reminder = plan.reminder_time if plan.reminder_time != old_reminder_time else False
            new_priority = plan.task_priority if plan.task_priority != old_priority else False
            new_week = plan.repeat_days if plan.repeat_days != old_week else False
            new_delta = plan.repeat_time_delta if plan.repeat_time_delta != old_delta else False
            new_count = plan.repeat_count if plan.repeat_count != old_count else False
            manager.edit_plan(user_login=user_login,
                              plan_id=plan_id,
                              new_name=new_name,
                              new_description=new_desc,
                              new_reminder_time=new_reminder,
                              new_priority=new_priority,
                              new_repeat=new_count,
                              new_week=new_week,
                              new_delta=new_delta)
            return HttpResponseRedirect(reverse('plan-edit', args=(plan_id,)))
    else:
        form = forms.PlanEditing(instance=plan)
    return render(request, 'plan_edit.html', { 'form': form,
                                               "user_login": user_login})


@exceptions_wrapper
@login_required
def plan_remove(request, plan_id):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    manager.delete_plan(user_login, plan_id)
    return HttpResponseRedirect(reverse('plans'))


@exceptions_wrapper
@login_required
def task_add_permission(request, task_id):
    if request.method == "POST":
        user_login = request.user.username
        manager = get_manager(user_login=user_login)
        allow_user_login = request.POST.get('allow_user_login')
        action_type = ActionType(int(request.POST.get('action_type')))
        if allow_user_login:
            if action_type == ActionType.READ:
                manager.task_allow_read(user_login=user_login, task_id=task_id, allow_user_login=allow_user_login)
            elif action_type == ActionType.WRITE:
                manager.task_allow_write(user_login=user_login, task_id=task_id, allow_user_login=allow_user_login)
            else:
                pass

    return HttpResponseRedirect(reverse('task-edit', args=(task_id,)))


@exceptions_wrapper
@login_required
def task_remove_permission(request, task_id, disallow_user):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    manager.task_disallow(user_login=user_login, task_id=task_id, disallow_user_login=disallow_user)

    return HttpResponseRedirect(reverse('task-edit', args=(task_id,)))


@exceptions_wrapper
@login_required
def filter_tasks(request):
    user_login = request.user.username
    manager = get_manager(user_login=user_login)
    if request.method == "POST":
        form = forms.FilterForm(request.POST)
        if form.is_valid():
            end_date = datetime.strftime(form.cleaned_data['end_date'], '%Y-%m-%d') if form.cleaned_data['end_date'] is not None else False
            name = form.cleaned_data['name'] if form.cleaned_data['name'] != '' else False
            status = form.cleaned_data['status']
            if status == str(len(TaskStatus)):
                status = False
            priority = form.cleaned_data['priority']
            if priority == str(len(TaskPriority)):
                priority = False
            is_archive = form.cleaned_data['is_archive'] if form.cleaned_data['is_archive'] is False else None
            filtered_tasks = manager.get_tasks(user_login=user_login,
                                      name=name,
                                      description=False,
                                      end_date=end_date,
                                      status=int(status) if status is not False else False,
                                      priority=int(priority) if priority is not False else False,
                                      is_archive=is_archive)
            return tasks(request, filtered_tasks)
    else:
        form = forms.FilterForm()
    return render(request, 'filter_page.html', {'form': form, 'user_status': user_login})
