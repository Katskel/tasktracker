from django.core.validators import RegexValidator
from django.contrib.auth.models import User
from django.db import models
from enumfields import EnumField
from lib.models.task_action import (ActionType)
from lib.models.task import (TaskPriority,
                             TaskStatus)


class UserInfo(models.Model):
    user_auth = models.OneToOneField(User, null=True, blank=True)
    user_login = models.CharField(max_length=64, primary_key=True)


class Task(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=128, null=True, blank=True)
    reminder_time = models.DurationField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    priority = EnumField(TaskPriority)
    status = EnumField(TaskStatus)
    head_task = models.ForeignKey('self', blank=True, null=True, on_delete=models.SET_NULL)
    is_archive = models.BooleanField()


class Group(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128)
    user = models.ForeignKey(UserInfo)
    tasks = models.ManyToManyField(Task, through='TaskGroup')


class TaskGroup(models.Model):
    task = models.ForeignKey(Task)
    group = models.ForeignKey(Group)


class TaskAction(models.Model):
    task = models.ForeignKey(Task, blank=True, null=True)
    user = models.ForeignKey(UserInfo, blank=True, null=True)
    action_type = EnumField(ActionType)


class Plan(models.Model):
    id = models.IntegerField(primary_key=True)
    task_name = models.CharField(max_length=128)
    end_date = models.DateTimeField()
    reminder_time = models.DurationField(blank=True, null=True)
    task_description = models.CharField(max_length=128, null=True, blank=True)
    task_priority = EnumField(TaskPriority)
    last_added = models.DateTimeField(blank=True, null=True)
    repeat_days = models.CharField(validators=[RegexValidator(regex='^1?2?3?4?5?6?7?$', message='Numbers from 1 to 7 (each number only once')],
                                max_length=7, blank=True, null=True)
    repeat_count = models.IntegerField(null=True, blank=True)
    repeat_time_delta = models.DurationField(null=True, blank=True)
    user = models.ForeignKey(UserInfo)
