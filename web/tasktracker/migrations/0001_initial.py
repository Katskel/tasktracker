# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-09-19 17:31
from __future__ import unicode_literals

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import enumfields.fields
import lib.models.task
import lib.models.task_action


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('task_name', models.CharField(max_length=128)),
                ('end_date', models.DateTimeField()),
                ('reminder_time', models.DurationField(blank=True, null=True)),
                ('task_description', models.CharField(blank=True, max_length=128, null=True)),
                ('task_priority', enumfields.fields.EnumField(enum=lib.models.task.TaskPriority, max_length=10)),
                ('last_added', models.DateTimeField(blank=True, null=True)),
                ('repeat_days', models.CharField(blank=True, max_length=7, null=True, validators=[django.core.validators.RegexValidator(message='Numbers from 1 to 7 (each number only once', regex='^1?2?3?4?5?6?7?$')])),
                ('repeat_count', models.IntegerField(blank=True, null=True)),
                ('repeat_time_delta', models.DurationField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=128)),
                ('description', models.CharField(blank=True, max_length=128, null=True)),
                ('reminder_time', models.DurationField(blank=True, null=True)),
                ('end_date', models.DateTimeField(blank=True, null=True)),
                ('priority', enumfields.fields.EnumField(enum=lib.models.task.TaskPriority, max_length=10)),
                ('status', enumfields.fields.EnumField(enum=lib.models.task.TaskStatus, max_length=10)),
                ('is_archive', models.BooleanField()),
                ('head_task', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='tasktracker.Task')),
            ],
        ),
        migrations.CreateModel(
            name='TaskAction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action_type', enumfields.fields.EnumField(enum=lib.models.task_action.ActionType, max_length=10)),
                ('task', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='tasktracker.Task')),
            ],
        ),
        migrations.CreateModel(
            name='TaskGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tasktracker.Group')),
                ('task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tasktracker.Task')),
            ],
        ),
        migrations.CreateModel(
            name='UserInfo',
            fields=[
                ('user_login', models.CharField(max_length=64, primary_key=True, serialize=False)),
                ('user_auth', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='taskaction',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='tasktracker.UserInfo'),
        ),
        migrations.AddField(
            model_name='plan',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tasktracker.UserInfo'),
        ),
        migrations.AddField(
            model_name='group',
            name='tasks',
            field=models.ManyToManyField(through='tasktracker.TaskGroup', to='tasktracker.Task'),
        ),
        migrations.AddField(
            model_name='group',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tasktracker.UserInfo'),
        ),
    ]
