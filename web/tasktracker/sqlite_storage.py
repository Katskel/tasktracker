from datetime import datetime, timedelta
from tasktracker import models
from lib.models.task import Task
from lib.models.task_planner import TaskPlanner
from lib.models.group import Group
from lib import exceptions
from django.db.models import Max
import pytz


class SQLiteStorage:
    def init(self):
        pass

    def clear_storage(self):
        pass

    def get_task_by_id(self, task_id):
        try:
            task = models.Task.objects.get(id=task_id)
        except models.Task.DoesNotExist:
            raise exceptions.TaskNotFound(task_id)
        lib_task = convert_task_from_model(task)
        return lib_task

    def check_task_action(self, task_id, user_login):
        try:
            task = models.Task.objects.get(id=task_id)
        except models.Task.DoesNotExist:
            raise exceptions.TaskNotFound(task_id)
        try:
            user = models.UserInfo.objects.get(user_login=user_login)
        except models.UserInfo.DoesNotExist:
            raise exceptions.UserNotFound(user_login)
        task_action = models.TaskAction.objects.filter(user=user, task=task)
        if len(task_action) > 0:
            return models.TaskAction.objects.get(user=user, task=task).action_type
        else:
            return None

    def update_task(self, task_id, task):
        model_task = convert_task_to_model(task)
        model_task.save()

    def get_user_tasks(self, user_login, name=False, description=False, end_date=False, status=False, priority=False, is_archive=None):
        tasks = models.Task.objects.all()
        if name is not False:
            tasks = tasks.filter(name__icontains=name.lower())
        if description is not False:
            tasks = tasks.filter(description=description)
        if end_date is not False:
            date = datetime.strptime(end_date, '%Y-%m-%d')
            tasks = tasks.filter(end_date__range=[date, date+timedelta(days=1)])
        if status is not False:
            tasks = tasks.filter(status=status)
        if priority is not False:
            tasks = tasks.filter(priority=priority)
        if is_archive is not None:
            tasks = tasks.filter(is_archive=is_archive)
        lib_tasks = []
        for task in tasks:
            if self.check_task_action(task.id, user_login) is not None:
                lib_task = convert_task_from_model(task)
                lib_tasks.append(lib_task)
        return lib_tasks

    def add_task(self, task):
        self.update_task(task.id, task)

    def add_task_action(self, task_action):
        self.update_task_action(task_action.task_id, task_action.user_login, task_action.action_type)

    def delete_task_action(self, task_id, user_login):
        try:
            task = models.Task.objects.get(id=task_id)
        except models.Task.DoesNotExist:
            raise exceptions.TaskNotFound(task_id)
        try:
            user = models.UserInfo.objects.get(user_login=user_login)
        except models.UserInfo.DoesNotExist:
            raise exceptions.UserNotFound(user_login)
        try:
            action = models.TaskAction.objects.get(task=task, user=user)
        except models.TaskAction.DoesNotExist:
            raise exceptions.TaskActionNotFound(task_id, user_login)
        action.delete()

    def update_task_action(self, task_id, user_login, task_action):
        try:
            task = models.Task.objects.get(id=task_id)
        except models.Task.DoesNotExist:
            raise exceptions.TaskNotFound(task_id)
        try:
            user = models.UserInfo.objects.get(user_login=user_login)
        except models.UserInfo.DoesNotExist:
            raise exceptions.UserNotFound(user_login)
        model_action = models.TaskAction(task=task, user=user, action_type=task_action)
        model_action.save()

    def get_task_subtasks(self, task_id):
        try:
            task = models.Task.objects.get(id=task_id)
        except models.Task.DoesNotExist:
            raise exceptions.TaskNotFound(task_id)
        subtasks = models.Task.objects.filter(head_task=task)
        lib_subtasks = []
        for subtask in subtasks:
            lib_subtask = convert_task_from_model(subtask)
            lib_subtasks.append(lib_subtask)
        return lib_subtasks

    def add_group(self, group):
        self.update_group(group.id, group)

    def update_group(self, group_id, group):
        model_group = convert_group_to_model(group)
        model_group.save()

    def delete_group(self, group_name, user_login):
        try:
            user = models.UserInfo.objects.get(user_login=user_login)
        except models.UserInfo.DoesNotExist:
            raise exceptions.UserNotFound(user_login)
        try:
            group = models.Group.objects.get(user=user,
                                         name=group_name)
        except models.Group.DoesNotExist:
            raise exceptions.GroupNotFound(group_name)
        group.delete()

    def get_user_groups(self, user_login):
        try:
            user = models.UserInfo.objects.get(user_login=user_login)
        except models.UserInfo.DoesNotExist:
            raise exceptions.UserNotFound(user_login)
        groups = models.Group.objects.filter(user=user)
        lib_groups = []
        for group in groups:
            lib_group = convert_group_from_model(group)
            lib_groups.append(lib_group)
        return lib_groups

    def get_group_by_name(self, group_name, user_login):
        try:
            user = models.UserInfo.objects.get(user_login=user_login)
        except models.UserInfo.DoesNotExist:
            raise exceptions.UserNotFound(user_login)
        try:
            group = models.Group.objects.get(user=user,
                                         name=group_name)
        except models.Group.DoesNotExist:
            raise exceptions.GroupNotFound(group_name)
        return convert_group_from_model(group)

    def get_group_by_id(self, group_id, user_login):
        try:
            user = models.UserInfo.objects.get(user_login=user_login)
        except models.UserInfo.DoesNotExist:
            raise exceptions.UserNotFound(user_login)
        try:
            group = models.Group.objects.get(user=user,
                                         id=group_id)
        except models.Group.DoesNotExist:
            raise exceptions.GroupNotFound(group_name)
        return convert_group_from_model(group)

    def get_group_tasks(self, user_login, group_name):
        try:
            user = models.UserInfo.objects.get(user_login=user_login)
        except models.UserInfo.DoesNotExist:
            raise exceptions.UserNotFound(user_login)
        try:
            group = models.Group.objects.get(user=user,
                                         name=group_name)
        except models.Group.DoesNotExist:
            raise exceptions.GroupNotFound(group_name)
        tasks = group.tasks.all()
        lib_tasks = []
        for task in tasks:
            lib_task = convert_task_from_model(task)
            lib_tasks.append(lib_task)

        return lib_tasks

    def update_plan(self, plan_id, plan):
        model_plan = convert_plan_to_model(plan)
        model_plan.save()

    def add_plan(self, plan):
        self.update_plan(plan.id, plan)

    def delete_plan(self, plan_id):
        try:
            plan = models.Plan.objects.get(id=plan_id)
        except models.Plan.DoesNotExist:
            raise exceptions.PlanNotFound(plan_id)
        plan.delete()

    def get_plan_by_id(self, plan_id):
        try:
            plan = models.Plan.objects.get(id=plan_id)
        except models.Plan.DoesNotExist:
            raise exceptions.PlanNotFound(plan_id)
        lib_plan = convert_plan_from_model(plan)
        return lib_plan

    def add_task_group(self, task_group):
        try:
            task = models.Task.objects.get(id=task_group.task_id)
        except models.Task.DoesNotExist:
            raise exceptions.TaskNotFound(task_group.task_id)
        try:
            group = models.Group.objects.get(id=task_group.group_id)
        except models.Group.DoesNotExist:
            raise exceptions.GroupNotFound(task_group.group_id)
        link = models.TaskGroup(task=task, group=group)
        link.save()

    def delete_task_group(self, task_id, group_id):
        try:
            task = models.Task.objects.get(id=task_id)
        except models.Task.DoesNotExist:
            raise exceptions.TaskNotFound(task_id)
        try:
            group = models.Group.objects.get(id=group_id)
        except models.Group.DoesNotExist:
            raise exceptions.GroupNotFound(group_id)
        link = models.TaskGroup.objects.get(task=task, group=group)
        link.delete()

    def get_max_task_id(self):
        result = models.Task.objects.all().aggregate(Max('id'))
        max = result['id__max'] if result['id__max'] is not None else 1
        return max

    def get_max_plan_id(self):
        result = models.Plan.objects.all().aggregate(Max('id'))
        max = result['id__max'] if result['id__max'] is not None else 1
        return max

    def get_max_group_id(self):
        result = models.Group.objects.all().aggregate(Max('id'))
        max = result['id__max'] if result['id__max'] is not None else 1
        return max

    def get_user_plans(self, user_login):
        try:
            user = models.UserInfo.objects.get(user_login=user_login)
        except models.UserInfo.DoesNotExist:
            raise exceptions.UserNotFound(user_login)
        plans = models.Plan.objects.filter(user=user)
        lib_plans = []
        for plan in plans:
            lib_plan = convert_plan_from_model(plan)
            lib_plans.append(lib_plan)
        return lib_plans

    def delete_task(self, task_id):
        try:
            task = models.Task.objects.get(id=task_id)
        except models.Task.DoesNotExist:
            raise exceptions.TaskNotFound(task_id)
        models.TaskGroup.objects.filter(task=task).delete()
        models.TaskAction.objects.filter(task=task).delete()
        task.delete()


def convert_task_to_model(lib_task):
    head_task = None
    if lib_task.head_task_id is not None:
        head_task = models.Task.objects.get(id=lib_task.head_task_id)
    model_task = models.Task(id=lib_task.id, name=lib_task.name, description=lib_task.description,
                             reminder_time=lib_task.reminder_time,
                             end_date=lib_task.end_date,
                             priority=lib_task.priority,
                             status=lib_task.status,
                             head_task=head_task,
                             is_archive=lib_task.is_archive)
    return model_task


def convert_task_from_model(model_task):
    lib_task = Task(task_id=model_task.id, name=model_task.name, description=model_task.description,
                    reminder_time=model_task.reminder_time,
                    end_date=(model_task.end_date if model_task.end_date is not None else None),
                    priority=model_task.priority,
                    head_task_id=model_task.head_task.id if model_task.head_task is not None else None,
                    status=model_task.status,
                    is_archive=model_task.is_archive)
    return lib_task


def convert_plan_from_model(model_plan):
    end_date = datetime.strftime(model_plan.end_date, '%Y-%m-%d %H:%M') if model_plan.end_date is not None else None
    added_date = datetime.strftime(model_plan.last_added, '%Y-%m-%d %H:%M') if model_plan.last_added is not None else None
    lib_plan = TaskPlanner(plan_id=model_plan.id,
                           task_name=model_plan.task_name,
                           end_date=(model_plan.end_date if model_plan.end_date is not None else None),
                           user_login=model_plan.user.user_login,
                           repeat_days=model_plan.repeat_days,
                           repeat_count=model_plan.repeat_count,
                           repeat_time_delta=model_plan.repeat_time_delta,
                           last_added=(model_plan.last_added if model_plan.last_added is not None else None),
                           reminder_time=model_plan.reminder_time,
                           task_description=model_plan.task_description,
                           task_priority=model_plan.task_priority)
    return lib_plan


def convert_plan_to_model(lib_plan):
    user = models.UserInfo.objects.get(user_login=lib_plan.user_login)
    model_plan = models.Plan(id=lib_plan.id,
                             task_name=lib_plan.task_name,
                             end_date=lib_plan.end_date,
                             task_description=lib_plan.task_description,
                             reminder_time=lib_plan.reminder_time,
                             task_priority=lib_plan.task_priority,
                             last_added=lib_plan.last_added,
                             repeat_days=lib_plan.repeat_days,
                             repeat_count=lib_plan.repeat_count,
                             repeat_time_delta=lib_plan.repeat_time_delta,
                             user=user)
    return model_plan


def convert_group_to_model(lib_group):
    user = models.UserInfo.objects.get(user_login=lib_group.user_login)
    model_group = models.Group(id=lib_group.id,
                               name=lib_group.name,
                               user=user)
    return model_group


def convert_group_from_model(model_group):
    lib_group = Group(group_id=model_group.id,
                      name=model_group.name,
                      user_login=model_group.user.user_login)
    return lib_group

