from django.http import HttpResponseRedirect
from django.views.generic.base import View
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.forms import ModelForm, Form
from tasktracker.models import UserInfo, Task, Group, Plan
from tasktracker.lib_connection import (get_task_priorities,
                             get_task_statuses)
from django.contrib.auth.models import User
from django import forms


class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = "/tasks/"
    template_name = "register.html"

    def form_valid(self, form):
        form.save()
        name = form.cleaned_data['username']
        UserInfo(user_login=name, user_auth=User.objects.get(username=name)).save()
        super(RegisterFormView, self).form_valid(form)
        login(self.request, User.objects.get(username=name))
        return HttpResponseRedirect("/tasks/")


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "login.html"
    success_url = "/tasks/"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        super(LoginFormView, self).form_valid(form)
        return HttpResponseRedirect("/tasks/")


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/login/')


class TaskEditing(ModelForm):
    class Meta:
        model = Task
        fields = ('name', 'description', 'end_date', 'reminder_time', 'priority')
        widgets = {
            'end_date': forms.DateTimeInput(attrs={'placeholder': 'YYYY-MM-DD HH:MM:SS'}),
        }


class GroupEditing(ModelForm):
    class Meta:
        model = Group
        fields = ('name', )


class PlanAdding(ModelForm):
    class Meta:
        model = Plan
        fields = ('task_name', 'task_description', 'end_date', 'repeat_time_delta',
                  'repeat_days', 'repeat_count', 'reminder_time', 'task_priority',)
        widgets = {
            'end_date': forms.DateTimeInput(attrs={'placeholder': 'YYYY-MM-DD HH:MM:SS'})
        }


class PlanEditing(ModelForm):
    class Meta:
        model = Plan
        fields = ('task_name', 'task_description', 'repeat_time_delta',
                  'repeat_days', 'repeat_count', 'reminder_time', 'task_priority',)


class FilterForm(Form):
    name = forms.CharField(max_length=50, required=False)
    end_date = forms.DateField(label='End date',
                               widget=forms.DateInput(attrs={'type': 'date'}),
                               required=False)
    priority = forms.ChoiceField(widget=forms.Select(),
                                 choices=get_task_priorities(),
                                 required=False)
    status = forms.ChoiceField(widget=forms.Select(),
                                 choices=get_task_statuses(),
                                 required=False)
    is_archive = forms.BooleanField(label='Show archive tasks', widget=forms.CheckboxInput(),
                                    required=False)
