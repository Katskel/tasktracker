from django.contrib import admin
from tasktracker.models import (Task,
                     TaskGroup,
                     TaskAction,
                     UserInfo,
                     Group,
                     Plan)


admin.site.register(Task)
admin.site.register(TaskGroup)
admin.site.register(TaskAction)
admin.site.register(UserInfo)
admin.site.register(Group)
admin.site.register(Plan)
